import React from 'react';

const Dashboard = React.lazy(() => import('./views/Menu/Dashboard/Dashboard'))
const ProjectDetail = React.lazy(() => import('./views/Menu/Project/ProjectDetail'))
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: Dashboard },
  { path: '/detail', exact: true, name: 'Project Detail', component: ProjectDetail}
];

export default routes;
