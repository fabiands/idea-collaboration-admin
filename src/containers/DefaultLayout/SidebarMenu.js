import { lazy } from "react";
import { t } from "react-switch-lang";

const Dashboard = lazy(() =>
  import("../../views/Menu/Dashboard/Dashboard")
)
const Project = lazy(() =>
  import("../../views/Menu/Project/Project")
)
const ProjectDetail = lazy(() =>
  import("../../views/Menu/Project/ProjectDetail")
)
const Notification = lazy(() =>
  import("../../views/Menu/Notification/Notification")
)
const Team = lazy(() =>
  import("../../views/Menu/Team/Team")
)
const Profile = lazy(() =>
  import("../../views/Menu/Profile/Profile")
)
const Sprint = lazy(() =>
  import("../../views/Menu/Team/DesignSprint")
)
const User = lazy(() =>
  import ("../../views/Menu/User/User")
)
const Admin = lazy(() =>
  import ("../../views/Menu/Admin/Admin")
)

export default () => [
  {
    url: "/dashboard",
    component: Dashboard,
    menu: {
      name: t("Dashboard"),
      icon: "icon-graph",
    },
  },
  {
    url: "/project",
    component: Project,
    exact: true,
    menu: {
      name: t("Proyek"),
      icon: "icon-briefcase",
    },
  },
  {
    url: "/notifications",
    component: Notification,
    menu: {
      name: t("Notifikasi"),
      icon: "icon-bell",
    },
  },
  {
    url: "/team",
    component: Team,
    exact: true,
    menu: {
      name: t("Tim"),
      icon: "icon-people",
    },
  },
  {
    url: "/user",
    component: User,
    exact: true,
    menu: {
      name: t("User"),
      icon: "icon-user",
    },
  },
  {
    url: "/manage-admin",
    component: Admin,
    exact: true,
    menu: {
      name: t("Manage Admin"),
      icon: "icon-people",
    },
  },
  {url: "/team/:code/sprint/:teamId", component: Sprint, exact: true},
  {url: "/project/detail/:id", component: ProjectDetail, exact:true},
  {url: "/Profile", component: Profile, exact: true}
];