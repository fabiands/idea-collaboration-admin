export default [
    {name: 'VP of Sales Marketing',
        slug: 'vp-of-sales-marketing',
        dueDate: '14 Agustus 2020',
        qualifications: [
            'Min. S1 Manajemen, Marketing, atau bidang terkait Business Development.',
            'Usia 20-30 tahun.',
            'Memiliki pengalaman bidang penjualan min. 4 tahun.',
            'Memiliki Pengalaman di bidang Consulting (diutamakan)',
            'Memiliki kemampuang dalam bidang leadership, strategic thinker, coaching, motivating, and develop strong relationships',
            'Memiliki pengetahuan bidang contracting, negotiating, and change management.',
            'Memiliki pengalaman dalam pengelolaan planning marketing strategies, advertising, dan publc relation.',
            'Memiliki kemampuan komunikasi verbal dan written yang baik.',
            'Diutamakan berdomisili di Yogyakarta'
        ], 
        responsibilities: [
            'Mengelola dan mengkoordinasi tim sales dan marketing',
            'Membuat strategi forecasting peningkatan penjulan dan profit goals',
            'Membuat marketing strategy mulai dari market research, pricing, product marketing, dll.',
            'Memastikan value-driven to customer tersampaikan dengan tepat.'
        ]
    },
    {name: 'Project Manager',
        slug: 'project-manager',
        dueDate: '14 Agustus 2020',
        qualifications: [
            'Min. D3/S1 Pemrograman, Teknik Informatika atau Bidang terkait Engineering.',
            'Usia 20-30 tahun.',
            'Memiliki pengalaman coding min. 2 tahun dan managing Team min. 1 tahun.',
            'Menguasai bahasa pemrograman PHP dan Javascript.',
            'Memahami framework Codeigniter dan ReactJS.',
            'Mengerti iterasi Development Mobile App.',
            'Memiliki kemampuan komunikasi verbal, dan written yang baik.',
            'Diutamakan berdomisili di Yogyakarta.',
        ], 
        responsibilities: [
            'Mengelola dan mengkoordinasi tim developer.',
            'Membuat strategi dan timeline project serta mengontrol setiap progress.',
            'Bekerja sama dengan tim teknologi untuk merancang fitur-fitur.',
            'Mengelola permasalahan dan resiko project dari segi teknologi.',
        ]
    },
    {name: 'UI/UX',
        slug: 'ui-ux', 
        dueDate: '14 Agustus 2020',
        qualifications: [
            'Min. D3/S1 semua jurusan.',
            'Usia 20-25 tahun.',
            'Memiliki pengalaman sebagai desain grafis min. 1 tahun.',
            'Memiliki pengalaman bidang UI/UX min. 1 tahun.',
            'Menguasai Adobe Desing Software seperti Adobe XD, Adobe Illustrator, dan Adobe Photoshop.',
            'Diutamakan memiliki skill interaction design, user research, wireframing, usability testing, dan tertarik dalam bidang Human Computer Interaction.',
            'Multitasking dan dapat menyelesaikan pekerjaan secara konsisten.',
            'Mampu bekerja sama dengan tim dan memiliki kemampuang komunikasi yang baik.'

        ], 
        responsibilities: [
            'Menginterpretasikan kebutuhan user dan Menerjemahkan dalam Requirements Product.',
            'Membuat wireframes, interaction models, arsitektur informasi, desain visual, dan dokumentasi pendukung.',
            'Membuat User-Friendly Interface dengan mengikuti kemajuan UI dan teknik terbaik.',
            'Berkolaborasi dengan tim dan stakeholder untuk memastikan desain tersampaikan dengan baik.'
        ],
    },
    {name: 'Fullstack Engineer',
        slug: 'fullstack-engineer',
        dueDate: '26 Agustus 2020',
        qualifications: [
            'Min. D3/S1 Teknik Informatika, Komputer, Sistem Informasi atau bidang terkait.',
            'Usia 20-30 tahun.',
            'Memiliki pengalaman coding min. 1 tahun.',
            'Menguasai bahasa pemrograman PHP dan Javascript.',
            'Menguasai framework Codeigniter dan ReactJS.',
            'Familiar dengan teknologi database seperti MySQL, Oracle dan MongoDB.',
            'Dapat mengoperasikan server linux seperti CentOS atau Ubuntu (Diutamakan).',
        ], 
        responsibilities: [
            'Mengembangkan arsitektur frontend website.',
            'Mendesain UI/UX.',
            'Mengembangkan aplikasi backend website.',
            'Mendeploy dan database.',
            'memastikan optimalisasi cross-platform untuk mobile apps.',
            'Bekerja disamping graphic designers untuk mengembangkan web.',
            'Mendesain dan mengembangkan API.'
        ]
    },
    {name: 'Front-End Engineer',
        slug:'frontend-engineer', 
        dueDate: '26 Agustus 2020',
        qualifications: [
            'Min. D3/S1 Pemrograman, Teknik Informatika, atau Bidang terkait Engineering.',
            'Usia 20-30 tahun.',
            'Memiliki pengalaman sebagai Front End',
            'Menguasai bahasa pemrograman Javascript',
            'Memahami framework Codeigniter dan ReactJS',
            'Mengerti iterasi development Mobile App.',
            'Memiliki kemampuan komunikasi verbal dan written yang baik.',
            'Lebih diutamakan domisili di Yogyakarta.'
        ], 
        responsibilities: [
            'Menulis kode website menggunakan Javascript, CSS atau HTML.',
            'Memastikan tidak ada bug dalam website atau aplikasi yang sedang dikembangkan.',
            'Membuat fitur nampak muka yang meningkatkan user experience.',
            'Memastikan keseimbangan antara fitur fungsional dengan estetis di website.',
            'Memastikan desain web kompatibel untuk dilihat dari smartphone.'
        ]
    },
    {name: 'IOS Developer',
        slug: 'ios-developer',
        dueDate: '26 Agustus 2020',
        qualifications: [
            'Min. D3/S1 semua jurusan.',
            'Usia 20-25 Tahun.',
            'Memiliki Pengalaman sebagai IOS Developer Min. 1 Tahun.',
            'Mengetahui IOS framework seperti Core Data, Core Animation, dll.',
            'Familiar dengan RESTful APIs.',
            'Mengetahui arsitektur aplikasi seperti Viper, MVP, MVVM.',
            'Memahami prinsip desain Apple dan interface guidelines.',
            'Multitasking dan dapat menyelesaikan pekerjaan secara konsisten.',
            'Diutamakan berdomisili di Yogyakarta.'
        ], 
        responsibilities: [
            'Mengembangkan teknologi terbaru untuk meningkatkan kualitas, keseimbangan, dan peningkatan performa aplikasi.',
            'Bertanggung jawab dalam penggambaran, pembangunan, dan pengimplementasian fitur produk pada IOS.',
            'Terlibat dalam proses pengembangan dan support mobile application menggunakan variasi teknologi dan layanan untuk menyediakan konsumen pengalaman mobile terbaik.',
            'Berkolaborasi dengan tim developer.'
        ]
    },
];