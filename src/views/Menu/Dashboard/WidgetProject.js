import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Card, Col, Row, Spinner } from 'reactstrap'
import request from '../../../utils/request'

function WidgetProject(){
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(null)
    const verified = data?.filter(item => item.verified === 'verified') ?? null
    const unverified = data?.filter(item => item.verified === 'pending') ?? null
    const rejected = data?.filter(item => item.verified === 'rejected') ?? null

    useEffect(() => {
        setLoading(true)
        request.get('v1/projects')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => console.log(err))
            .finally(() => setLoading(false))
    }, [])

    return(
        <Row style={{marginLeft:"-5px"}}>
            <Col className="p-2">
                <Card className="bg-white border-0 text-center px-2 py-3" style={{borderRadius:'12px'}}>
                    <i className="my-1 fa fa-check-circle text-success" style={{fontSize:'6.5rem'}} /><br />
                    <strong className="mt-1 widget-project-strong">Proyek Disetujui</strong>
                    {verified ?
                        <span className="text-secondary my-1">
                            {verified.length} Proyek
                        </span>
                    : <Spinner className="mx-auto my-1" color="secondary" size="sm" />
                    }
                    <div className="text-center my-3">
                    <Link to={'/project#accepted'} disabled={loading} className="button-reset btn btn-sm btn-outline-netis-primary px-3 mx-auto">
                        Lihat Semua
                    </Link>
                    </div>
                </Card>
            </Col>
            <Col className="p-2">
                <Card className="bg-white border-0 text-center px-2 py-3" style={{borderRadius:'12px'}}>
                    <i className="my-1 fa fa-times-circle text-danger" style={{fontSize:'6.5rem'}} /><br />
                    <strong className="mt-1 widget-project-strong">Proyek Ditolak</strong>
                    {rejected ?
                        <span className="text-secondary my-1">
                            {rejected.length} Proyek
                        </span>
                    : <Spinner className="mx-auto my-1" color="secondary" size="sm" />
                    }
                    <div className="text-center my-3">
                    <Link to={'/project#rejected'} disabled={loading} className="button-reset btn btn-sm btn-outline-netis-primary px-3 mx-auto">
                        Lihat Semua
                    </Link>
                    </div>
                </Card>
            </Col>
            <Col className="p-2">
                <Card className="bg-white border-0 text-center px-2 py-3" style={{borderRadius:'12px'}}>
                    <i className="my-1 fa fa-question-circle text-warning" style={{fontSize:'6.5rem'}} /><br />
                    <strong className="mt-1 widget-project-strong">Proyek Menunggu<br />Verifikasi</strong>
                    {unverified ?
                        <span className="text-secondary my-1">
                            {unverified.length} Proyek
                        </span>
                    : <Spinner className="mx-auto my-1" color="secondary" size="sm" />
                    }
                    <div className="text-center my-3">
                    <Link to={'/project#unverified'} disabled={loading} className="button-reset btn btn-sm btn-outline-netis-primary px-3 mx-auto">
                        Lihat Semua
                    </Link>
                    </div>
                </Card>
            </Col>
        </Row>
    )
}

export default WidgetProject