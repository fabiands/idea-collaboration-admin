import React from 'react'
import Skeleton from 'react-loading-skeleton'
import { Card, CardBody, Row, Col } from 'reactstrap'

function SkeletonTeam(){
    return(
        <section>
            <Card className="rounded bg-white border-0 py-3" style={{borderRadius:'12px'}}>
                <CardBody className="mb-md-5">
                    {Array(3).fill().map((item, idx) => (
                        <Row key={idx} className="my-1">
                            <Col xs="3" className="d-flex align-items-center justify-content-center">
                                <Skeleton circle={true} height={40} width={40} />
                            </Col>
                            <Col xs="9" className="d-flex align-items-center justify-content-start pl-2">
                                <Skeleton height={30} width={130} />
                            </Col>
                        </Row>
                    ))}
                </CardBody>
            </Card>
        </section>
    )
}

export default SkeletonTeam