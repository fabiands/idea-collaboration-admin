import React, { useEffect, useState } from 'react'
import { Badge, Card, CardBody, Col, Row, Table } from 'reactstrap'
import GoogleMapReact from 'google-map-react';
// import request from '../../../utils/request';
import MapMarker from '../../../components/MapMarker';
import CarouselProject from '../Project/Carousel/CarouselProject';
import { Link } from 'react-router-dom';
import StatisticMap from './StatisticMap';
import request from '../../../utils/request';
import SkeletonTeam from './Skeleton/SkeletonTeam';

const center = {
    lat: -5.548455891225692,
    lng: 121.78471029513555
}
const badge = {
    registration: 'warning',
    ideation: 'success',
    finish: 'info'
}

function WidgetMap() {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [activeData, setActiveData] = useState(null)
    const [activeId, setActiveId] = useState(0)

    const onMarkerClick = (item, idx) => {
        setActiveId(idx);
        setActiveData(item);
    }

    useEffect(() => {
        request.get('v1/projects?verified=verified&statusTeams=approved')
            .then((res) => {
                setData(res.data.data)
                setActiveData(res.data.data[0])
            })
            .catch((err) => console.log(err))
            .finally(() => setLoading(false))
    }, [])

    if (loading) {
        return <SkeletonTeam />
    }

    console.log(activeData)
    return (
        <Row>
            <Col md="8">
                <Row>
                    <Col xs="12">
                        <Card className="bg-white border-0 px-3 py-2" style={{ borderRadius: '12px' }}>
                            <div style={{ height: '50vh', width: '100%' }}>
                                <GoogleMapReact
                                    bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                    center={center}
                                    defaultZoom={5}
                                >
                                    {data?.map((item, idx) => (
                                        <MapMarker
                                            onClick={() => onMarkerClick(item, idx)}
                                            key={idx}
                                            lat={parseFloat(item.locationLatitude) || 0}
                                            lng={parseFloat(item.locationLongitude) || 0}
                                            text={""}
                                            active={activeId === idx ? true : false}
                                            data={item}
                                        />
                                    ))}
                                </GoogleMapReact>
                            </div>
                        </Card>
                    </Col>
                    <Col xs="12" className="d-none d-md-block">
                        <StatisticMap />
                    </Col>
                </Row>
            </Col>
            <Col md="4">
                <Card className="bg-white border-0 table-widget" style={{ borderRadius: '12px' }}>
                    <CardBody>
                        <Row>
                            <Col sm="6" md="12">
                                <CarouselProject data={activeData?.media} height="25vh" type="picture-frame-small" />
                            </Col>
                            <Col sm="6" md="12">
                                <Link to={`/project/detail/${activeData?.code}`} className="text-dark">
                                    <h5 className="mt-4 mb-2">{activeData?.title}</h5>
                                </Link>
                                <Table borderless className="mb-1 mb-0">
                                    <tbody>
                                        <tr>
                                            <th scope="row" className="w-25 p-1 pl-0">Lokasi</th>
                                            <td className="p-1">{activeData?.locationName}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" className="w-25 p-1 pl-0">Status</th>
                                            <td className="p-1 text-capitalize">
                                                <Badge color={badge[activeData?.status]}>
                                                    {activeData?.status}
                                                </Badge>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" className="w-25 p-1 pl-0">Tim</th>
                                            <td className="p-1">{activeData?.teams.length}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
            <Col xs="12" className="d-md-none">
                <StatisticMap />
            </Col>
        </Row>
    )
}

export default WidgetMap