import React from 'react'
import { Col, Row } from 'reactstrap'
import WidgetMap from './WidgetMap'
import WidgetProject from './WidgetProject'
import WidgetTeam from './WidgetTeam'

function Dashboard(){
  return(
    <div className="px-md-3">
      <Row>
        <Col md="8">
          <h4>Dashboard</h4>
          <WidgetProject />
        </Col>
        <Col md="4">
          <h4>Tim</h4>
          <WidgetTeam />
        </Col>
      </Row>
      <h4>Persebaran Proyek</h4>
      <WidgetMap />
    </div>
  )
}

export default Dashboard