import React, { Fragment, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { Card, CardFooter, CardBody, Row, Col } from 'reactstrap'
import ModalError from '../../../components/ModalError';
import request from '../../../utils/request';
import profilePhotoNotFound from '../../../assets/img/no-photo.png';
// import {team} from "../Team/dummyTeam";
import SkeletonTeam from './Skeleton/SkeletonTeam';

function WidgetTeam(){
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [data, setData] = useState(null)

    useEffect(() => {
        request.get('v1/teams?status=pending')
        .then((res) => {
            let item = res.data.data
            if(item.length <= 3){
                // console.log('gausah di slice')
                setData(res.data.data)
            }
            else if(item.length > 3){
                // console.log('pake di slice')
                const slice = item.slice(0,3)
                setData(slice)
            }
        })
        .catch(() => setError(true))
        .finally(() => setLoading(false))
    }, [])

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }


    if(!data){
        if (loading){
            return <SkeletonTeam />
        }
        if (error){
            return <ModalError />
        }
    }

    return(
        <Card className="rounded bg-white border-0 p-2" style={{borderRadius:'12px'}}>
            <CardBody className="mb-md-3">
                {data?.length <1 ?
                    <span><i>Belum ada permintaan verifikasi Tim</i></span>
                :
                data?.map((item, idx) => (
                    <Fragment key={idx}>
                        <Row className="border border-dark hover py-2" style={{borderRadius:'8px'}}>
                            <Col xs="3" className="pt-1 rounded-circle d-flex justify-content-center align-items-start">
                                {item?.lead?.leadPhoto ? 
                                    <img src={item.lead.leadPhoto} width={40} height={40} alt="profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                    :
                                    <img src={require('../../../assets/img/no-photo.png')} width={30} height={30} alt="profile" />
                                }
                            </Col>
                            <Col xs="9">
                                <strong>{item?.lead?.leadName}</strong><br />
                                <span className="text-secondary">{item?.members?.length}&nbsp;Anggota</span><br />
                                <small><i>{item?.project?.title}</i></small>
                            </Col>
                        </Row>
                        <Row className="d-flex flex-row-reverse">
                            <Col xs="12"><hr /></Col>
                        </Row>
                    </Fragment>
                ))}
            </CardBody>
            <CardFooter className="bg-white border-top-0 text-center mt-md-2">
                <Link to={'/team#pending'} className="button-reset btn btn-sm btn-outline-netis-primary px-3 mx-auto">
                    Lihat Semua
                </Link>
            </CardFooter>
        </Card>
    )
}

export default WidgetTeam