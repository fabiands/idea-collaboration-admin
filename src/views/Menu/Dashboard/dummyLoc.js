export const data = [
    {
        id:1,
        lat:-7.748632215555999,
        lng:110.36186698614962,
        title: 'Jalan Disana',
        loc: 'Jl. Adipati Luhung Blok B No 2B, Sleman',
        code: "4fb8e7b2f59b87f7a047bb29ad1d3b04",
        team: 4,
        status: "ideation",
        media: [
            {
                "id": 10,
                "projectId": 4,
                "fileName": "c34a2275bb414f23ee251ec333ffd921.jpeg",
                "mimeType": "image/jpeg",
                "storage": "https://appolo-dev.widyaskilloka.com/files/media/projects/1/c34a2275bb414f23ee251ec333ffd921.jpeg",
                "createdAt": "2021-05-16T20:32:47.000Z",
                "updatedAt": "2021-05-16T20:32:47.000Z"
            },
            {
                "id": 11,
                "projectId": 4,
                "fileName": "0032e634fac921a38c6d7f0cb520df4d.jpeg",
                "mimeType": "image/jpeg",
                "storage": "https://appolo-dev.widyaskilloka.com/files/media/projects/1/0032e634fac921a38c6d7f0cb520df4d.jpeg",
                "createdAt": "2021-05-16T20:32:47.000Z",
                "updatedAt": "2021-05-16T20:32:47.000Z"
            },
            {
                "id": 12,
                "projectId": 4,
                "fileName": "ff6f0925c218c876ebe1221e8e8bec84.jpeg",
                "mimeType": "image/jpeg",
                "storage": "https://appolo-dev.widyaskilloka.com/files/media/projects/1/ff6f0925c218c876ebe1221e8e8bec84.jpeg",
                "createdAt": "2021-05-16T20:32:47.000Z",
                "updatedAt": "2021-05-16T20:32:47.000Z"
            }
        ]
    },
    {
        id:2,
        lat:-1.4873793262716695,
        lng:103.0416952029755,
        title: 'Perbaikan Jalan',
        loc: 'Jl. in aja dulu nanti bakal rusak juga hubungan kita',
        code: "4fb8e7b2f59b87f7a047bb29ad1d3b04",
        team: 3,
        status: "ideation"
    },
    {
        id:3,
        lat:-1.4287383676186725,
        lng:133.13425100597436,
        title: 'Rumah Rusak',
        loc: 'Rumah kita sendiri',
        code: "4fb8e7b2f59b87f7a047bb29ad1d3b04",
        team: 6,
        status: "finish",
    },
    {
        id:4,
        lat:0.03761239343378239,
        lng:114.18708624112323,
        title: 'Saya rusak anak orang',
        loc: 'Pasar Kembang v3.1',
        code: "4fb8e7b2f59b87f7a047bb29ad1d3b04",
        team: 2,
        status: "registration",
    }
]