import React, { useEffect, useMemo, useState } from 'react'
import { Card, Progress } from 'reactstrap'
import request from '../../../utils/request'
import SkeletonTeam from './Skeleton/SkeletonTeam'

function StatisticMap() {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        request.get('v1/projects/statistics')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => console.log(err))
            .finally(() => setLoading(false))
    }, [])

    const totalValue = useMemo(() => data ? data.reduce(function (total, item) {
        return total + item.total;
    }, 0) : 0, [data]);

    if (loading) {
        return <SkeletonTeam />
    }

    return (
        <Card className="bg-white border-0 px-3 py-2" style={{ borderRadius: '12px' }}>
            {data?.map((item, idx) => (
                <div key={idx}>
                    <Progress value={(item.total / totalValue) * 100} />
                    <div className="d-flex justify-content-between my-1">
                        <small>{item.locationProvince}</small>
                        <small className="text-info">{item.total}</small>
                    </div>
                </div>
            ))}
        </Card>
    )
}

export default StatisticMap