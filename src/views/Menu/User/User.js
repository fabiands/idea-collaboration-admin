import React, { useCallback, useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom';
import { Col, Input, Nav, NavItem, NavLink, Row, Spinner, TabContent, TabPane } from 'reactstrap'
import request from '../../../utils/request'
import UserList from './UserList';

const tabs = {
    'basic' : 'User Aktif',
    'blocked' : 'User Non-aktif'
}
const tabsArray = Object.keys(tabs);

function User(){
    const [loadingBasic, setLoadingBasic] = useState(false)
    const [loadingBlocked, setLoadingBlocked] = useState(false)
    const [dataBasic, setDataBasic] = useState(null)
    const [dataBlocked, setDataBlocked] = useState(null)
    const [basic, setBasic] = useState(null)
    const [blocked, setBlocked] = useState(null)
    const data = ((dataBasic || dataBlocked) && dataBasic?.concat(dataBlocked)) ?? null
    const [filter, setFilter] = useState(null)
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    const getBasic = useCallback(() => {
        setLoadingBasic(true)
        return request.get('v1/users')
            .then((res) => {
                setBasic(res.data.data)
                setDataBasic(res.data.data)
            })
            .catch((err) => console.log(err))
            .finally(() => setLoadingBasic(false))
    },[])

    const getBlocked = useCallback(() => {
        setLoadingBlocked(true)
        return request.get('v1/users?status=blocked')
            .then((res) => {
                setBlocked(res.data.data)
                setDataBlocked(res.data.data)
            })
            .catch((err) => console.log(err))
            .finally(() => setLoadingBlocked(false))
    },[])

    const getData = useCallback(() => {
        getBasic();
        getBlocked();
    }, [getBasic, getBlocked])

    useEffect(() => {
            setBlocked(filter?.filter(item => item?.status === 'blocked') ?? null)
            setBasic(filter?.filter(item => item?.status === 'basic') ?? null)
    }, [filter])

    const searchData = (e) => {
        const {value} = e.target;
        if(value){
            let filtered = data.filter(item => item.detail.fullName.toLowerCase().includes(value.toLowerCase()))
            setFilter(filtered)
        }
        else {
            setFilter(data)
        }
    }

    useEffect(() => {
        getData()
    }, [getData])

    return(
        <div className="p-2 px-lg-4 project-tab">
            <h4>Daftar User</h4>
            <Row className="mb-3 mb-md-0 d-flex flex-row-reverse">
                <Col md="4" lg="3" className="input-tab">
                    <div className="relative-input search-addon">
                        <Input id="search" name="search" type="input" onChange={searchData} placeholder="Search..." className="form-control" />
                        <i className="fa fa-search icon-inside-left text-netis-primary" />
                    </div>
                </Col>
                <Col md="8" lg="9">
                    <Nav tabs>
                        {tabsArray.map(tab => (
                            <NavItem key={tab}>
                                <NavLink tag={Link} className="pt-2/5" active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                                    {tabs[tab]}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
            </Row>
            <TabContent activeTab={selectedTab}>
                <TabPane tabId="basic">
                    {(!basic || loadingBasic) ? 
                        <div className="text-center my-3">
                            <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
                        </div>
                        :
                        <UserList key={basic} data={basic} getData={getData} stat="basic" />
                    }
                </TabPane>
                <TabPane tabId="blocked">
                    {(!blocked || loadingBlocked) ? 
                        <div className="text-center my-3">
                            <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
                        </div>
                        :
                        <UserList key={blocked} data={blocked} getData={getData} stat="blocked" />
                    }
                </TabPane>
            </TabContent>
        </div>
    )
}

export default User