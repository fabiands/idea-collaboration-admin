export const team = [
    {
        id:1,
        chief: "Daniel Leinad",
        member: ["Budi", "Badu", "Bodo"],
        color: 'info',
        status: 'unverified',
        createdAt: '2021-01-11'
    },
    {
        id:2,
        chief: "Anto Onta",
        member: ["Merah", "Abang"],
        color: 'success',
        status: 'unverified',
        createdAt: '2021-05-16'
    }
]