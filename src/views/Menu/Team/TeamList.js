import React, { useState } from 'react'
import { Badge, Button, Col, Form, Input, Label, ListGroup, ListGroupItem, Modal, ModalBody, ModalFooter, ModalHeader, Row, Spinner, Table } from 'reactstrap'
import DataNotFound from "../../../components/DataNotFound";
import ProfilePhotoNotFound from '../../../assets/img/no-photo.png'
import { Link } from 'react-router-dom';
import * as moment from 'moment'
import request from '../../../utils/request';
import { toast } from 'react-toastify';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';

const badge = {
    registration: 'warning',
    ideation: 'success',
    finish: 'info'
}
const statDesc = {
    approved: 'memverifikasi',
    rejected: 'menolak'
}
const statBadge = {
    approved: 'success',
    pending: 'warning',
    rejected: 'danger'
}

function TeamList({ data, getData, stat, member }) {
    const [modalData, setModalData] = useState(null)
    const [verify, setVerify] = useState(null)
    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState(null)
    const [more, setMore] = useState(false)
    const [loadingMember, setLoadingMember] = useState(false)
    const [memberData, setMemberData] = useState(null)
    const [memberError, setMemberError] = useState(false)

    const verifyTeam = () => {
        setLoading(true)
        request.put(`v1/projects/${modalData.project.code}/team/${modalData.id}`, {
            status: verify,
            message: message
        })
            .then(() => {
                toast.success(`Berhasil ${statDesc[verify]} Tim`)
                setVerify(null)
                setModalData(false)
                getData()
            })
            .catch(() => {
                toast.error('Gagal mengubah verifikasi data')
                return;
            })
            .finally(() => setLoading(false))
    }

    const getMember = (id) => {
        setLoadingMember(true)
        request.get(`v1/teams/${id}/members?status=${member}`)
            .then((res) => {
                setMemberData(res.data.data)
            })
            .catch((err) => {
                console.log(err)
                setMemberError(true)
            })
            .finally(() => setLoadingMember(false))
    }

    const onAvatarError = (e) => {
        const img = e.target;
        img.onerror = null;
        img.src = ProfilePhotoNotFound;
        img.style.border = null;
    }

    if (data.length < 1) {
        return <DataNotFound />
    }
    return (
        <>
            <Table hover responsive>
                <thead>
                    <tr className="bg-white border-bottom-1">
                        <th style={{ width: '40px' }}></th>
                        <th className="text-center">Nama Tim</th>
                        <th className="text-center">Jumlah Anggota</th>
                        <th className="text-left">Proyek</th>
                        {stat === 'pending' && <th></th>}
                    </tr>
                </thead>
                <tbody>
                    {data?.map((item, idx) => (
                        <tr key={idx}>
                            <td style={{ width: '40px', paddingLeft: '3px', paddingRight: '3px' }} className="text-center">
                                {item?.lead?.leadPhoto ?
                                    <img src={item.lead.leadPhoto} alt="profile" width={30} height={30} style={{ objectFit: 'cover' }} onError={(e) => onAvatarError(e)} className="rounded-circle border" />
                                    :
                                    <DefaultProfile init={item.lead.leadName} size="30px" />
                                }
                            </td>
                            <td>
                                <strong>{item?.lead?.leadName}</strong>
                            </td>
                            <td className="text-center">{item?.members?.length + 1}</td>
                            <td>
                                <Link to={`/project/detail/${item.project.code}`} className="text-dark">
                                    <b>{item?.project?.title}</b>
                                </Link>
                            </td>
                            <td className="text-nowrap">
                                <Button
                                    color="netis-primary"
                                    onClick={() => {
                                        setModalData(item)
                                        getMember(item.id)
                                    }}
                                    className="px-3" style={{ borderRadius: '8px' }}
                                >
                                    Lihat Detail
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Modal
                className="right"
                isOpen={modalData ? true : false}
                toggle={() => setModalData(null)}
            >
                <ModalHeader className="text-capitalize modal-team-header" style={{ position: 'relative' }}>
                    {stat === 'pending' && 'Pengajuan'} Tim {modalData?.lead?.leadName}
                    <br />
                    <Badge color={statBadge[modalData?.status]}>
                        {modalData?.status}
                    </Badge>
                    <div className="absolute-right">
                        {modalData?.lead?.leadPhoto ?
                            <img src={modalData?.lead?.leadPhoto} alt="profile" width={35} height={35} style={{ objectFit: 'cover' }} onError={(e) => onAvatarError(e)} className="rounded-circle border" />
                            :
                            <DefaultProfile init={modalData?.lead?.leadName} size="35px" />
                        }
                    </div>
                </ModalHeader>
                <ModalBody className="pt-1">
                    <h4>Proyek : </h4>
                    <h6>{modalData?.project?.title}</h6>
                    <Table hover className="mb-1 mb-0">
                        <tbody>
                            <tr>
                                <th scope="row" className="w-25 p-1 pl-0">Status</th>
                                <td className="p-1">
                                    <Badge color={badge[modalData?.project?.status]}>
                                        {modalData?.project?.status}
                                    </Badge>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="w-25 p-1 pl-0">Lokasi</th>
                                <td className="p-1">{modalData?.project?.locationName ?? "-"}</td>
                            </tr>
                            {modalData?.status !== 'pending' && <tr>
                                <th scope="row" className="w-25 p-1 pl-0">Berlangsung Hingga</th>
                                <td className="p-1">{moment(modalData?.project?.statusUntil).format('Do MMMM YYYY') ?? "-"}</td>
                            </tr>}
                        </tbody>
                    </Table>
                    {/* <Link to={`/project/detail/${modalData?.project?.code}`}>
                    <small>Lihat selengkapnya...</small>
                </Link> */}
                    <hr />
                    <h4>Jumlah Anggota : </h4>
                    {loadingMember ? <><Spinner size="xs" color="secondary" className="mr-2" /><span className="text-secondary ml-2"><i>Memuat...</i></span></>
                    :
                    memberError ?
                    <span className="text-danger">Terjadi kesalahan pemuatan anggota, silahkan muat ulang</span>
                    :
                    <>
                        <h6 className="text-capitalize">{memberData?.length} anggota</h6>
                        <ListGroup>
                            {memberData?.map((item, idx) => (
                                <ListGroupItem key={idx} className={(idx >= 8 && !more) ? 'd-none' : ''} style={{position:'relative'}}>
                                    {modalData?.lead?.leadId === item?.user?.id &&
                                    <Badge className="absolute-right-small text-white" color="info">Ketua Tim</Badge>
                                    }
                                    <Row>
                                        <Col xs="3" className="text-center pt-2">
                                            {item?.user?.photo ?
                                                <img src={item?.user?.photo} alt="profile" width={30} height={30} style={{ objectFit: 'cover' }} onError={(e) => onAvatarError(e)} className="rounded-circle border" />
                                                :
                                                <DefaultProfile init={item?.user?.fullName} size="30px" className="mx-auto" />
                                            }
                                        </Col>
                                        <Col xs="9" className="pt-1">
                                            <span className="text-capitalize">
                                                {item?.user?.fullName}
                                            </span><br />
                                            <b>Solusi : </b>
                                            { item?.solving?.message ? <i>"{item?.solving?.message}"</i> : " - " }
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            ))}
                            {memberData?.length >= 8 ?
                                more ?
                                    <Button onClick={() => setMore(false)} className="mt-1 mx-auto btn-sm" color="mute" style={{ borderRadius: '8px' }}>
                                        <i className="fa fa-lg fa-angle-up mr-2" /> Sembunyikan
                                    </Button>
                                    :
                                    <Button onClick={() => setMore(true)} className="mt-1 mx-auto btn-sm" color="mute" style={{ borderRadius: '8px' }}>
                                        <i className="fa fa-lg fa-angle-down mr-2" /> Selengkapnya
                                    </Button>
                                : null
                            }
                        </ListGroup>
                    </>
                    }
                    {modalData?.status !== 'pending' &&
                        <div className="mt-2">
                            {modalData?.status === 'rejected' ?
                                <>
                                    <hr />
                                    <h5>Alasan Penolakan : </h5>
                                    <h6>{modalData?.statusMessage ?? "-"}</h6>
                                </>
                                :
                                modalData?.project?.status !== 'registration' ?
                                    <Link to={{
                                        pathname: `/team/${modalData?.project?.code}/sprint/${modalData?.id}`,
                                        state: {
                                            project: modalData?.project?.title,
                                            team: modalData?.lead?.leadName,
                                            stat: modalData?.project?.status
                                        }
                                    }}
                                        className="btn btn-netis-primary px-3 mt-2"
                                        style={{ borderRadius: '10px' }}
                                    >
                                        <i className="fa fa-lightbulb-o mr-2" />Lihat Design Sprint
                                    </Link>
                                    :
                                    null
                            }
                        </div>
                    }

                </ModalBody>
                {stat === 'pending' && <ModalFooter>
                    <div className="text-nowrap">
                        <Button disabled={loading} onClick={() => setVerify('approved')} color="netis-success" className="mr-2 px-2" style={{ borderRadius: '10px' }}>
                            <i className="fa fa-check mr-1" />Terima
                        </Button>
                        <Button disabled={loading} onClick={() => setVerify('rejected')} color="netis-danger" className="ml-2 px-2" style={{ borderRadius: '10px' }}>
                            <i className="fa fa-times mr-1" />Tolak
                        </Button>
                    </div>
                </ModalFooter>}
            </Modal>
            <Modal isOpen={verify ? true : false} style={{ marginTop: "40vh" }} toggle={() => setVerify(null)}>
                <ModalHeader className="border-bottom-0">
                    {verify === 'approved' ? 'Perubahan Status Verifikasi Tim' : 'Perubahan Status Penolakan Tim'}
                </ModalHeader>
                <ModalBody className="text-center py-2">
                    Apakah anda ingin {statDesc[verify]} Tim <b>"{modalData?.lead?.leadName}"</b> ?
                    {verify === 'rejected' &&
                        <Form className="mt-4 text-left">
                            <Label htmlFor="message" className="input-label">Berikan alasan penolakan Tim</Label>
                            <Input type="textarea" id="message" name="message" rows="3" onChange={(e) => setMessage(e.target.value)} />
                        </Form>
                    }
                </ModalBody>
                <ModalFooter className="border-top-0 text-center py-3">
                    <div className="text-center d-flex justify-content-between mx-auto mt-2" style={{ width: "50%" }}>
                        <Button onClick={verifyTeam} disabled={loading || (verify === 'rejected' && !message)} color="netis-success" className="mr-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            {loading ? <Spinner size="sm" className="mx-auto" /> : <><i className="fa fa-check mr-1" />Ya</>}
                        </Button>
                        <Button onClick={() => setVerify(null)} disabled={loading} color="netis-danger" className="ml-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            <i className="fa fa-times mr-1" />Batal
                        </Button>
                    </div>
                </ModalFooter>
            </Modal>
        </>
    )
}

export default TeamList