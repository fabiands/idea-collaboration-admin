import React from "react";
import { RatingPreview } from "./Components/Rating";
import { memo } from "react";
import DescriptionResult from "./Components/DescriptionResult";

export const ResultCard = memo(({ data }) => {
    return (
        <>
            <div className="sprint-desc">
                {data.values.description}
            </div>
            <div className={`${data.container !== 'prototyping' ? 'd-flex' : 'd-none'} justify-content-center mt-3 w-100`}>
                <RatingPreview data={data?.rating} />
            </div>
        </>
    )
})

export const ResultCardDetail = memo(({ data }) => {
    return (
        <div className="card-detail">
            <DescriptionResult item={data} />
        </div>
    )
})