import React from "react";
import { AttachmentsFixedPreview } from "./Components/AttachmentsFixed";
import { RatingPreview } from "./Components/Rating";

export const CrazyEightCard = ({ data }) => {
    return (
        <>
            <div className="sprint-desc">
                {data.values.description}
            </div>
            <div className="mt-3">
                <AttachmentsFixedPreview data={data?.attachments} cardId={data?.id} />
            </div>
            <div className="d-flex justify-content-center mt-3 w-100">
                <RatingPreview data={data?.rating} />
            </div>
        </>
    )
}