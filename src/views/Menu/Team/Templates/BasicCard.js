import React from "react";
import { AssignmentPriview } from "./Components/Assignments";
import { RatingPreview } from "./Components/Rating";
import { memo } from "react";

export const BasicCard = memo(({ data }) => {
    
    return (
        <>
            <div className="sprint-desc">
                {data.values.description}
            </div>
            <div className={`${data?.assignments.length > 0 && data.container === 'prototyping' ? 'd-flex' : 'd-none'} float-right my-3`}>
                {data?.assignments.map((ass, i) => (
                    <AssignmentPriview data={ass} key={i} />
                ))}
            </div>
            <div className={`${data.container !== 'prototyping' ? 'd-flex' : 'd-none'} justify-content-center mt-3 w-100`}>
                <RatingPreview data={data?.rating} />
            </div>
        </>
    )
})