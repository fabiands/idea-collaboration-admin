import React, { useState, memo, useCallback } from "react";
import { Row, Col, UncontrolledTooltip, Modal } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import blankPreview from '../../../../../assets/img/no-image-preview.png';

const AttachmentsFixed = memo(({ data }) => {

    return (
        <Row className="attach mb-4">
            <Col xs="1" className="px-0 d-flex align-items-center justify-content-center">
                <FontAwesomeIcon icon='paperclip' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
            </Col>
            <Col xs="11" className="px-0">
                <div className="d-flex align-items-center">
                    <h5 className={`font-weight-bold mb-0`}>Lampiran</h5>
                </div>
            </Col>
            <Col xs={{ size: 10, offset: 1 }} className="px-0 mt-3">
                <Row>
                    {data?.map((att, i) => (
                        <Col xs={`${data.length === 9 || data.length === 15 ? '4' : '3'}`} key={i}>
                            <Attachment data={att} key={i} />
                        </Col>
                    ))}
                </Row>
            </Col>
        </Row>
    )
})

export const AttachmentsFixedPreview = memo(({ data }) => {
    const onErrorAttachments = (e) => {
        e.target.src = blankPreview;
        e.target.onerror = null;
    }

    return (
        <Row className="attach mt-3">
            {data?.map((att, i) => (
                <Col xs={`${data.length === 9 || data.length === 15 ? '4' : (data.length === 1 ? '12' : '3')}`} key={i} className="px-0">
                    <div className="attach-image-fixed small d-flex justify-content-center align-items-center" style={{ height: `${data.length === 9 || data.length === 15 ? '45px' : (data.length === 1 ? '130px' : '45px')}` }}>
                        <img src={att?.values ?? ''} alt="attachments" onError={(e) => onErrorAttachments(e)} id={`popover-lampiran-delete-${att.id}`} />
                    </div>
                </Col>
            ))}
        </Row >
    )
})

const Attachment = memo(({ data }) => {
    const [showImage, setShowImage] = useState(false)

    const handleShowImage = useCallback(() => {
        setShowImage(!showImage)
    }, [showImage])

    const onErrorAttachments = useCallback((e) => {
        e.target.src = blankPreview;
        e.target.onerror = null;
    }, [])

    return (
        <div className="mb-3">
            <div className="attach-image-fixed mb-3 d-flex justify-content-center align-items-center">
                <img src={data?.values ?? ''} alt="attachments" onError={(e) => onErrorAttachments(e)} />
                <div
                    className="btn border-0 img-attach-button"
                    style={{ position: 'absolute', cursor: 'pointer' }}
                    onClick={handleShowImage}
                >
                    <div className="float-right">
                        <FontAwesomeIcon icon="eye" type="far" className="icon-show" onClick={handleShowImage} id={`popover-lampiran-show-${data.id}`} />
                        <UncontrolledTooltip placement="bottom" target={`popover-lampiran-show-${data.id}`}>
                            Lihat Gambar
                        </UncontrolledTooltip>
                    </div>
                </div>
            </div>
            <ShowImage data={data?.values} isShow={showImage} toggle={handleShowImage} onErrorAttachments={onErrorAttachments} />
        </div>
    )
})

const ShowImage = memo(({ data, isShow, toggle, onErrorAttachments }) => {
    const handleToggle = useCallback(() => {
        toggle(false)
    }, [toggle])

    return (
        <Modal isOpen={isShow} toggle={() => handleToggle()} size="lg" centered className="modal-show-image">
            <img src={data} alt="attachments-preview" onError={onErrorAttachments} style={{objectFit:'cover'}} />
        </Modal>
    )
})


export default AttachmentsFixed