import React, { memo } from "react";
import { Row, Col } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Rating from "./Rating";

export default memo(({ item }) => {
    const data = item.values

    return (
        <>
            <Row className="mb-4">
                <Col xs="1" className="px-0 d-flex align-items-center justify-content-center">
                    <FontAwesomeIcon icon='pager' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                </Col>
                <Col xs="8" className="px-0 d-flex align-items-center card-detail-title">
                    {data.title}
                </Col>
            </Row>
            <Row className="mb-4">
                <Col xs="6">
                    <Row className="mb-4">
                        <Col xs="2" className="px-0 d-flex align-items-center justify-content-center">
                            <FontAwesomeIcon icon='align-left' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                        </Col>
                        <Col xs="10" className="px-0 d-flex align-items-center">
                            <h5 htmlFor="description" className={`font-weight-bold mb-0`}>Deskripsi</h5>
                        </Col>
                        <Col xs={{ size: 10, offset: 2 }} className="px-0 pr-3 mt-1">
                            {data.description}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" style={{marginLeft:'20px'}}>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" style={{marginLeft:'20px'}}>
                            <Rating data={item.rating} result={true} />
                        </Col>
                    </Row>
                </Col>
                <Col xs="6">
                    <Row>
                        <Col xs="2" className="px-0 d-flex align-items-center justify-content-center">
                            <FontAwesomeIcon icon='business-time' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                        </Col>
                        <Col xs="10" className="px-0 d-flex align-items-center">
                            <h5 htmlFor="benefit" className={`font-weight-bold mb-0`}>Benefit</h5>
                        </Col>
                        <Col xs={{ size: 10, offset: 2 }} className="px-0 pr-3 mt-3">
                            {data.benefits}
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col xs="2" className="px-0 d-flex align-items-center justify-content-center">
                            <FontAwesomeIcon icon='money-bill' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                        </Col>
                        <Col xs="10" className="px-0 d-flex align-items-center">
                            <h5 htmlFor="benefit" className={`font-weight-bold mb-0`}>Biaya</h5>
                        </Col>
                        <Col xs={{ size: 10, offset: 2 }} className="px-0 pr-3 mt-3">
                            {data.cost}
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col xs="2" className="px-0 d-flex align-items-center justify-content-center">
                            <FontAwesomeIcon icon='people-carry' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                        </Col>
                        <Col xs="10" className="px-0 d-flex align-items-center">
                            <h5 htmlFor="benefit" className={`font-weight-bold mb-0`}>Sumber Daya</h5>
                        </Col>
                        <Col xs={{ size: 10, offset: 2 }} className="px-0 pr-3 mt-3">
                            {data.resource}
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col xs="2" className="px-0 d-flex align-items-center justify-content-center">
                            <FontAwesomeIcon icon='book-open' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                        </Col>
                        <Col xs="10" className="px-0 d-flex align-items-center">
                            <h5 htmlFor="benefit" className={`font-weight-bold mb-0`}>Kesimpulan</h5>
                        </Col>
                        <Col xs={{ size: 10, offset: 2 }} className="px-0 pr-3 mt-3">
                            {data.conclusion}
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
})