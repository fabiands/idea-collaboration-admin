import React, { useMemo, useState, useEffect, memo } from "react";
import { Row, Col, Progress } from "reactstrap";
import ReactStars from "react-rating-stars-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default memo(({ data, result }) => {

    const rate1 = useMemo(() => data[1] ?? [], [data])
    const rate2 = useMemo(() => data[2] ?? [], [data])
    const rate3 = useMemo(() => data[3] ?? [], [data])
    const rate4 = useMemo(() => data[4] ?? [], [data])
    const rate5 = useMemo(() => data[5] ?? [], [data])

    const rateCount = useMemo(() => parseInt(rate5.length) + parseInt(rate4.length) + parseInt(rate3.length) + parseInt(rate2.length) + parseInt(rate1.length), [rate5, rate4, rate3, rate2, rate1])
    const rateAmount = useMemo(() => ((5 * rate5.length) + (4 * rate4.length) + (3 * rate3.length) + (2 * rate2.length) + (1 * rate1.length)) / rateCount, [rate5, rate4, rate3, rate2, rate1, rateCount])

    const ratePercents = useMemo(() => (
        {
            1: rate1.length / rateCount * 100,
            2: rate2.length / rateCount * 100,
            3: rate3.length / rateCount * 100,
            4: rate4.length / rateCount * 100,
            5: rate5.length / rateCount * 100,
        }
    ), [rate5, rate4, rate3, rate2, rate1, rateCount])

    return (
        <Row className="attach my-3">
            <Col xs="1" className={`${result ? 'pl-3' : `px-0`} d-flex align-items-center justify-content-center`}>
                <FontAwesomeIcon icon='star' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
            </Col>
            <Col xs="11" className={result ? 'pl-3' : `px-0`}>
                <div className="d-flex align-items-center">
                    <h5 className={`font-weight-bold mb-0`}>Penilaian</h5>
                </div>
            </Col>
            <Col xs={{ size: 11, offset: 1 }} className={`${result ? '' : 'px-0'} mt-3`}>
                <Row>
                    <Col xs={result ? '3' : '2'} className="px-0 text-center">
                        <div className="font-weight-bold" style={{ fontSize: '50pt', lineHeight: 1 }}>{rateAmount ? (Math.round(rateAmount * 100) / 100).toFixed(1) : 0}</div>
                        <div style={{ fontSize: '12pt' }}>dari 5</div>
                    </Col>
                    <Col xs={result ? '7' : '5'} className="px-0 d-flex justify-content-start">
                        <div className="w-100" style={{ marginLeft: '-2rem', marginTop: '6px' }}>
                            <div className="d-flex align-items-center">
                                <div className="d-block" style={{ width: '40%' }}>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                </div>
                                <Progress color='secondary' value={ratePercents[5]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="d-block" style={{ width: '40%' }}>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                </div>
                                <Progress color='secondary' value={ratePercents[4]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="d-block" style={{ width: '40%' }}>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                </div>
                                <Progress color='secondary' value={ratePercents[3]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="d-block" style={{ width: '40%' }}>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                </div>
                                <Progress color='secondary' value={ratePercents[2]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="d-block" style={{ width: '40%' }}>
                                    <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                </div>
                                <Progress color='secondary' value={ratePercents[1]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                            </div>

                            <div className="w-100 mt-1">
                                <div className="float-right">{rateCount} Penilaian</div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
})

export const RatingPreview = memo(({ data }) => {
    const [starKeyForce, setStarKeyForce] = useState(0)

    const rate1 = useMemo(() => data[1] ?? [], [data])
    const rate2 = useMemo(() => data[2] ?? [], [data])
    const rate3 = useMemo(() => data[3] ?? [], [data])
    const rate4 = useMemo(() => data[4] ?? [], [data])
    const rate5 = useMemo(() => data[5] ?? [], [data])

    const rateCount = useMemo(() => parseInt(rate5.length) + parseInt(rate4.length) + parseInt(rate3.length) + parseInt(rate2.length) + parseInt(rate1.length), [rate5, rate4, rate3, rate2, rate1])
    const rateAmount = useMemo(() => ((5 * rate5.length) + (4 * rate4.length) + (3 * rate3.length) + (2 * rate2.length) + (1 * rate1.length)) / rateCount, [rate5, rate4, rate3, rate2, rate1, rateCount])

    useEffect(() => {
        setStarKeyForce(prev => prev + 1)
    }, [rateAmount])

    return (
        <>
            <ReactStars
                count={5}
                size={16}
                value={rateAmount ? rateAmount : 0}
                edit={false}
                isHalf={true}
                emptyIcon={<i className="fa fa-star"></i>}
                halfIcon={<i className="fa fa-star-half-alt"></i>}
                fullIcon={<i className="fa fa-star"></i>}
                activeColor="#ffd700"
                key={starKeyForce}
            />
        </>
    )
})