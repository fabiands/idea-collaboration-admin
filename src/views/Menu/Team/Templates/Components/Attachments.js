import React, { memo } from "react";
import { Row, Col } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import blankImage from '../../../../../assets/img/no-project.png';
import * as moment from 'moment';

const Attachments = memo(({ data, container }) => {

    return (
        <Row className="attach my-3">
            <Col xs="1" className={`${container === 'result' ? 'pl-3' : `px-0`} d-flex align-items-center justify-content-center`}>
                <FontAwesomeIcon icon='paperclip' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
            </Col>
            <Col xs="11" className={container === 'result' ? '' : 'px-0'}>
                <div className="d-flex align-items-center">
                    <h5 className={`font-weight-bold mb-0`}>Lampiran</h5>
                </div>
            </Col>
            <Col xs={{ size: 11, offset: 1 }} className={`${container === 'result' ? '' : 'px-0'} mt-3`}>
                {data?.length < 1 ?
                    <div className="text-muted">Tidak ada lampiran yang diberikan.</div>
                : data?.map((att, i) => (
                        <Attachment data={att} key={i} />
                    ))
                }
            </Col>
        </Row>
    )
})

const Attachment = memo(({ data }) => {
    const type = data?.type === 'file' && data.values.split('.')
    const fileType = type[type.length-1]

    const onErrorAttachments = (e) => {
        e.target.src = blankImage;
        e.target.onerror = null;
    }

    return (
        <div className="mb-3 d-flex align-items-center" style={{position:'relative'}}>
            {data?.type === 'link' ?
                <div style={{position:'absolute'}} className="attachment-icon-link">
                    <i className="fa fa-2x fa-code link attachment-icon-link" />
                </div>
                :
                (fileType === 'png' || fileType === 'jpg' || fileType === 'jpeg' || fileType === 'gif') ?
                    ''
                :
                    <div style={{position:'absolute'}} className="attachment-icon-file" onClick={() => console.log(fileType)}>
                        <i className="fa fa-2x fa-file attachment-icon-file" /><br />
                        <span>{fileType}</span>
                    </div>
                
            }
            <img src={data.type === 'file' ? data.values : ''} alt="attachments" onError={(e) => onErrorAttachments(e)} className="attach-image" />
            <div className="ml-3">
                <a href={data.values} className="text-dark font-weight-bold" target="_blank" rel="noopener noreferrer">{data.title} <FontAwesomeIcon icon="external-link-alt" size="sm" className="ml-1" /> </a>
                <div className="text-muted d-flex">
                    Ditambahkan {moment(data.createdAt).format("DD MMMM YYYY")} pukul {moment(data.createdAt).format("HH:mm")}
                </div>
            </div>
        </div>
    )
})

export default Attachments