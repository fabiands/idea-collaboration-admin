import React, { useState } from "react";
import { Row, Col, Popover, PopoverBody } from "reactstrap";
import noPhoto from '../../../../../assets/img/no-photo.png';
import { memo } from "react";

const Assignments = memo(({ data }) => {

    return (
        <Row className="mb-4 assignment">
            <Col xs={{ size: 11, offset: 1 }} className="px-0">
                <div className="d-flex align-items-center">
                    <div className={`font-weight-bold mb-0 font-md text-muted`}>Anggota</div>
                </div>
            </Col>
            <Col xs={{ size: 11, offset: 1 }} className="mt-2 px-0">
                <div className="mb-3 d-flex align-items-center">
                    {data.length <= 0
                        ? <div className="text-muted">Tidak ada anggota yang diberi tugas.</div>
                        : data?.map((act, i) => (
                            <Assignment data={act} id={i} key={i} />
                        ))
                    }
                </div>
            </Col>
        </Row>
    )
})

const Assignment = memo(({ data, id }) => {
    const [popOverDelete, setPopOverDelete] = useState(false)

    const onErrorAssignmentImage = (e) => {
        e.target.src = noPhoto;
        e.target.onerror = null;
    }

    return (
        <div className="mr-2">
            <img
                className="rounded-circle"
                style={{ width: '40px', height: '40px', objectFit: 'cover', cursor: "pointer" }}
                src={data?.photo ?? noPhoto} alt="User"
                id={`popover-assigment-${id}`}
                onError={(e) => onErrorAssignmentImage(e)}
            />
            <Popover trigger="legacy" placement="bottom" target={`popover-assigment-${id}`} style={{ minWidth: '250px' }} isOpen={popOverDelete} toggle={() => setPopOverDelete(!popOverDelete)}>
                <PopoverBody>
                    <div className="mb-3 d-flex align-items-center">
                        <img src={data?.photo ?? noPhoto} alt="User" onError={(e) => onErrorAssignmentImage(e)} className="rounded-circle" style={{ width: '40px', height: '40px', objectFit: 'cover' }} />
                        <div className="ml-3">
                            <h6 className="text-dark font-weight-bold mb-0">{data?.fullName}</h6>
                        </div>
                    </div>
                </PopoverBody>
            </Popover>
        </div>
    )
})

export const AssignmentPriview = memo(({ data }) => {
    const onErrorAssignmentImage = (e) => {
        e.target.src = noPhoto;
        e.target.onerror = null;
    }

    return (
        <div className="mr-2">
            <img
                className="rounded-circle"
                style={{ width: '25px', height: '25px', objectFit: 'cover', cursor: "pointer" }}
                src={data?.photo ?? noPhoto} alt="User"
                onError={(e) => onErrorAssignmentImage(e)}
            />
        </div>
    )
})

export default Assignments