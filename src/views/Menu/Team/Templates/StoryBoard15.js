import React from "react";
import { AssignmentPriview } from "./Components/Assignments";
import { AttachmentsFixedPreview } from "./Components/AttachmentsFixed";
import { RatingPreview } from "./Components/Rating";

export const StoryBoard15 = ({ data }) => {
    return (
        <>
            <div className="sprint-desc">
                {data.values.description}
            </div>
            <div className="mt-3">
                <AttachmentsFixedPreview data={data?.attachments} cardId={data?.id} />
            </div>
            <div className={`${data?.assignments.length > 0 ? 'd-flex' : 'd-none'} float-right my-3 w-100`}>
                {data?.assignments.map((ass, i) => (
                    <AssignmentPriview data={ass} key={i} />
                ))}
            </div>
            <div className="d-flex justify-content-center mt-3 w-100">
                <RatingPreview data={data?.rating} />
            </div>
        </>
    )
}
