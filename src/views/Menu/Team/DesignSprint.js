import React, { useEffect, useMemo, useState } from 'react'
import { useLocation, useRouteMatch } from 'react-router-dom'
import { Card, CardBody, CardHeader, Table, Spinner, Badge, Collapse, Button } from 'reactstrap'
import ModalError from '../../../components/ModalError'
import request from '../../../utils/request'
import SprintCard from './Colums/SprintCard'

const badge = {
    ideation: 'success',
    finish: 'info'
}

function DesignSprint(){
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [info, setInfo] = useState(false)
    const match = useRouteMatch()
    const location = useLocation()

    useEffect(() => {
        setLoading(true)
        request.get(`v1/teams/${match.params.teamId}/cards`)
        .then((res) => {
            setData(res.data.data)
        })
        .catch(() => {
            setError(true)
        })
        .finally(() => setLoading(false))
    }, [match])

    const dataAnalysis = useMemo(() => data?.filter((d) => d.category === 'idealist').concat(data?.filter((d) => d.category === 'analysis')), [data])
    const dataPrototyping = useMemo(() => data?.filter((d) => d.category === 'todo').concat(data?.filter((d) => d.category === 'inprogress'), data?.filter((d) => d.category === 'done')), [data])
    const dataResult = useMemo(() => data?.filter((d) => d.category === 'result'), [data])

    if(error){
        return <ModalError isOpen={true} />
    }

    return(
        <Card className="design-sprint shadow-sm border-0 ml-3">
            <CardHeader className="design-sprint-header">
                <div className="ml-2">
                    <h5>
                        Proyek {location.state.project ?? "PP Appolo"}&nbsp;&nbsp;-&nbsp;&nbsp;
                        <small className="text-secondary"><i>oleh Tim {location.state.team ?? "Anonim"}</i></small><br />
                        <i className="fa fa-info-circle text-info mt-2 mr-2 hover" onClick={() => setInfo(true)} />
                        <Badge color={badge[location.state.stat]}>
                            {location.state.stat}
                        </Badge>
                    </h5>
                    <Collapse isOpen={info} className="w-75">
                        {location.state.stat === 'ideation' ?
                            <>
                                Tahap ini masih dalam pengembangan dan data yang terdapat pada Design Sprint masih dapat 
                                berubah-ubah berdasarkan pengembangan Tim ini.
                                <Button className="btn-sm ml-2 py-0 px-2 text-white" color="info" style={{borderRadius:'8px'}} onClick={() => setInfo(false)}>Mengerti</Button>
                            </>
                        :
                            <>
                                Tahap ini merupakan tahap akhir dalam pengembangan Design Sprint. Silahkan melakukan penilaian pada 
                                Design Sprint setiap Tim.
                                <Button className="btn-sm ml-2 py-0 px-2 text-white" color="info" style={{borderRadius:'8px'}} onClick={() => setInfo(false)}>Mengerti</Button>
                            </>
                        }
                    </Collapse>
                </div>
            </CardHeader>
            <CardBody className='p-0'>
                {loading ?
                    <div
                        style={{
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            background: "rgba(255,255,255, 0.5)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            height: "75vh",
                        }}
                    >
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                    :
                    <Table borderless responsive className="table-sprint mb-0">
                        <tbody>
                            <tr>
                                <td className="pl-4"><SprintCard title="Analisis ide" data={dataAnalysis} stat={location.state.stat} /></td>
                                <td className="px-3"><SprintCard title="Prototyping" data={dataPrototyping} stat={location.state.stat} /></td>
                                <td className="pr-4"><SprintCard title="Result" data={dataResult} stat={location.state.stat} /></td>
                            </tr>
                        </tbody>
                    </Table>
                }
            </CardBody>
        </Card>
    )
}

export default DesignSprint