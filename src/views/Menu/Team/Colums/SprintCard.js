import React, {useCallback, useState} from 'react'
import { Badge, Card, CardBody, CardHeader } from 'reactstrap';
import ModalDetailCard from '../ModalDetailCard';
import { BasicCard } from "../Templates/BasicCard";
import { CrazyEightCard } from "../Templates/CrazyEightCard";
import { FishBone } from "../Templates/FishBone";
import { ResultCard } from '../Templates/ResultCard';
import { SprintMap } from "../Templates/SprintMap";
import { StoryBoard15 } from '../Templates/StoryBoard15';
import { StoryBoard9 } from "../Templates/StoryBoard9";

function SprintCard({title, data, stat}){
    const [modalCard, setModalCard] = useState(false)
    const [modalCardData, setModalCardData] = useState(null)
    console.log(stat)

    const toggleModalCard = useCallback((e) => {
        setModalCard(false)
        setModalCardData(null)
    }, [])

    return(
        <>
        <Card className="mt-3 border-0 rounded" style={{minWidth:'330px'}}>
            <CardHeader className="text-center border-bottom-0 rounded-top" style={{ backgroundColor: '#EFEEEE' }}>
                <div className="text-uppercase font-weight-bold font-lg">{title}</div>
            </CardHeader>
            <CardBody className="d-flex justify-content-around rounded-bottom" style={{ backgroundColor: '#EFEEEE', minHeight: '65vh' }}>
                {data?.length < 1 ?
                    <Card>
                        <CardBody>
                            {stat === 'ideation' ?
                                <span>
                                    Belum ada Data yang dapat ditampilkan. <br />
                                    Status Proyek ini masih bersifat&nbsp;
                                    <Badge color="success">Ideasi</Badge>&nbsp;
                                    sehingga Design Sprint Tim ini masih dalam tahap pengembangan. Silahkan cek kembali
                                    Design Sprint Tim ini ketika status Proyek sudah bersifat <Badge color="finish">Finish</Badge>
                                </span>
                                :
                                <span>
                                    Tidak ada Data yang dapat ditampilkan.<br />
                                    Status Proyek ini sudah bersifat&nbsp;
                                    <Badge color="info">Finish</Badge>&nbsp;
                                    sehingga pengembangan Design Sprint Tim ini sudah selesai.
                                </span>
                            }
                        </CardBody>
                    </Card>
                :
                data?.map((el, id) => (
                    <div
                        key={id}
                        className={`sprint-box ${title === 'Hasil' ? 'overflow-hidden' : 'overflow-auto'}`}
                    >
                        <div className="sticky-header text-center text-uppercase">
                            {el.category}
                        </div>
                        {el.cards.map((item, index) => (
                            <div
                                key={index}
                                className="get-item-sprint-card"
                                onClick={() => {
                                    setModalCard(true)
                                    setModalCardData(item)
                                }}
                            >
                                <Card className="px-0 bg-transparent border-0 mb-0" style={{ position: 'relative' }}>
                                    <CardHeader className="border-bottom-0 bg-transparent text-left p-1 px-2 w-75">
                                        <strong>{item.values.title}</strong>
                                    </CardHeader>
                                    <CardBody className="p-1 sprint-content px-2">
                                        {item.template === 'basic' && <BasicCard data={item} />}
                                        {item.template === 'c8' && <CrazyEightCard data={item} />}
                                        {item.template === 'fishbone' && <FishBone data={item} />}
                                        {item.template === 'sprintmap' && <SprintMap data={item} />}
                                        {item.template === 'storyboard9' && <StoryBoard9 data={item} />}
                                        {item.template === 'storyboard15' && <StoryBoard15 data={item} />}
                                        {item.template === 'result' && <ResultCard data={item} />}
                                    </CardBody>
                                </Card>
                            </div>
                        ))}
                    </div>
                ))}
            </CardBody>
        </Card>
        {modalCardData &&
            <ModalDetailCard isOpen={modalCard} toggle={toggleModalCard} data={modalCardData} title={title} />
        }
        </>
    )
}

export default SprintCard