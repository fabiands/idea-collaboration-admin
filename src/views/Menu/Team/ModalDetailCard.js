import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useCallback } from "react";
import { memo } from "react";
import { Col, Modal, ModalBody, Row } from "reactstrap";
import Assignments from "./Templates/Components/Assignments";
import Attachments from "./Templates/Components/Attachments";
import AttachmentsFixed from "./Templates/Components/AttachmentsFixed";
import Rating from "./Templates/Components/Rating";
import { ResultCardDetail } from "./Templates/ResultCard";

export default memo(({ isOpen, toggle, data, title }) => {
    const handleToggle = useCallback(() => {
        toggle(false)
    }, [toggle])

    return (
        <Modal isOpen={isOpen} toggle={() => handleToggle()} size={data?.template === 'result' ? 'xl' : 'lg'}>
            <ModalBody className="py-4 px-3 card-detail" style={{ minHeight: '90vh' }}>
                <div type="button" className="close p-3" aria-label="Close" onClick={() => handleToggle()} style={{ border: 0, position: 'absolute', top: '0px', right: '0px' }}><span aria-hidden="true">×</span></div>
                {data?.template === 'result' ?
                    <ResultCardDetail data={data} />
                :
                    <>
                        <Row className={title === 'Prototyping' ? 'mb-3' : 'mb-5'}>
                            <Col xs="1" className="px-0 d-flex align-items-center justify-content-center">
                                <FontAwesomeIcon icon='pager' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                            </Col>
                            <Col xs="8" className="px-0 d-flex align-items-center text-capitalize card-detail-title">
                                {data.values.title}
                            </Col>
                        </Row>
                        {title === 'Prototyping' && 
                            <Assignments data={data?.assignments} />
                        }
                        <Row className="my-3">
                            <Col xs="1" className="px-0 d-flex align-items-center justify-content-center">
                                <FontAwesomeIcon icon='align-left' className="font-weight-bold" style={{ color: '#42526e', fontSize: '14pt' }} />
                            </Col>
                            <Col xs="11" className="px-0 d-flex align-items-center">
                                <h5 htmlFor="description" className={`font-weight-bold mb-0`}>Deskripsi</h5>
                            </Col>
                            <Col xs={{ size: 10, offset: 1 }} className="px-0 mt-1">
                                {data.values.description}
                            </Col>
                        </Row>
                        {data?.template === 'basic' ?
                            <Attachments write="true" data={data?.attachments} cardId={data?.id} container={data?.container} />
                        :
                            <AttachmentsFixed write="true" data={data?.attachments} cardId={data?.id} />
                        }
                        {title === 'Analisis ide' &&
                            <Rating data={data?.rating} />
                        }
                    </>
                }
            </ModalBody>
        </Modal>
    )
})