import React, { useMemo, useState, useEffect } from 'react'
import { Col, Input, Button, Row, Table, Card, CardBody, Spinner, Modal, ModalHeader, ModalBody, ModalFooter, Label } from 'reactstrap'
import useSWR from 'swr';
import ProfilePhotoNotFound from '../../../assets/img/no-photo.png'
import DataNotFound from '../../../components/DataNotFound';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile'
import ModalError from '../../../components/ModalError'
import Select from 'react-select'
import request from '../../../utils/request'
import { toast } from 'react-toastify';

function Admin() {
    const { data: getData, error, mutate } = useSWR('v1/users?role=admin');
    const {data: getUser, error: errorUser, mutate: mutateUser} = useSWR('v1/users')
    const loading = !getData || error || !getUser || errorUser
    const data = useMemo(() => getData?.data?.data ?? [], [getData]);
    const dataUser = useMemo(() => getUser?.data?.data ?? [], [getUser]);
    const [filter, setFilter] = useState([])
    const [addAdmin, setAddAdmin] = useState(false)
    const [addId, setAddId] = useState([])
    const [deleting, setDeleting] = useState(null)
    const [submitting, setSubmitting] = useState(false)
    const optionUser = dataUser?.map((item) => ({
        value: item.id,
        label: item.detail.fullName
    }));
    
    useEffect(() => {
        if(data){
            setFilter(data)
        }
    }, [data])

    const submitAdmin = () => {
        setSubmitting(true)
        request.post('v1/users/manage-admin', {
            userId: addId,
            role: 'admin'
        })
        .then(() => {
            toast.success('Berhasil Menambahkan akun Admin')
            setAddAdmin(false)
            mutate()
            mutateUser()
        })
        .catch(() => {
            toast.error('Gagal Menambahkan akun Admin')
            return;
        })
        .finally(() => setSubmitting(false))
    }
    const deleteAdmin = () => {
        setSubmitting(true)
        request.post('v1/users/manage-admin', {
            userId: [deleting?.id],
            role: 'user'
        })
        .then(() => {
            mutateUser()
            setFilter(filter.filter(item => item.id !== deleting?.id))
            setDeleting(false)
        })
        .catch(() => {
            toast.error('Gagal Menghapus akun Admin')
            return;
        })
        .finally(() => setSubmitting(false))
    }

    const searchData = (e) => {
        const { value } = e.target;
        if(value){
            let filtered = data.filter(item => item.detail.fullName.toLowerCase().includes(value.toLowerCase()))
            setFilter(filtered)
        }
        else {
            setFilter(data)
        }
    }
    const inputUser = (e) => {
        let user = e?.map(item => item.value)
        setAddId(user)
    }

    const onAvatarError = (e) => {
        const img = e.target;
        img.onerror = null;
        img.src = ProfilePhotoNotFound;
        img.style.border = null;
    }

    if(loading){
        return (
            <div style={{width:'100vw', height:'100vh'}} className="d-flex justify-content-center align-items-center">
                <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
            </div>
        )
    }

    if(error || errorUser){
        return <ModalError isOpen={true} />
    }

    return (
        <div className="p-2 px-lg-4 project-tab">
            <h4>Daftar Admin</h4>
            <Card className="shadow-sm border-0 mt-3">
                <CardBody>
                    <Row className="mb-3 mb-md-0 d-flex flex-row-reverse">
                        <Col xs="12" className="input-tab mb-3 d-flex justify-content-between">
                            <Button color="netis-primary" className="px-2" style={{ borderRadius: '5px' }} onClick={() => setAddAdmin(true)}><i className="fa fa-plus mr-1" />Tambah</Button>
                            <div className="relative-input search-addon">
                                <Input id="search" name="search" type="input" onChange={searchData} placeholder="Search..." className="form-control" />
                                <i className="fa fa-search icon-inside-left text-netis-primary" />
                            </div>
                        </Col>
                        <Col xs="12">
                            {filter.length < 1 ? <DataNotFound />
                            :
                            <Table hover responsive>
                                <thead>
                                    <tr className="bg-white border-bottom-1">
                                        <th style={{ width: '40px' }}></th>
                                        <th className="text-left">Nama</th>
                                        <th className="text-left">No Telepon</th>
                                        <th className="text-left">Email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                {filter?.map((item, idx) => (
                                    <tr key={idx}>
                                        <td style={{ width: '40px', paddingLeft: '3px', paddingRight: '3px' }} className="text-center">
                                            {item?.detail?.photo ?
                                                <img src={item.detail.photo} alt="profile" width={30} height={30} style={{ objectFit: 'cover' }} onError={(e) => onAvatarError(e)} className="rounded-circle border" />
                                                :
                                                <DefaultProfile init={item.detail.fullName} size="30px" />
                                            }
                                        </td>
                                        <td>
                                            <strong>{item?.detail?.fullName}</strong>
                                        </td>
                                        <td className="text-left">{item?.detail?.phoneNumber}</td>
                                        <td><i>{item?.email}</i></td>
                                        <td className="text-nowrap">
                                            <Button onClick={() => setDeleting(item)} color="netis-primary" className="px-3" style={{ borderRadius: '8px' }}>
                                                Hapus
                                            </Button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                            }
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <Modal isOpen={addAdmin} size="lg">
                <ModalHeader toggle={() => setAddAdmin(false)} className="border-bottom-0">
                    Penambahan akun Admin
                </ModalHeader>
                <ModalBody>
                    <Label htmlFor="user" className="input-label">Daftar User</Label>
                    <Select
                        isMulti
                        name="user"
                        onChange={inputUser}
                        options={optionUser}
                        className="basic-multi-select"
                        classNamePrefix="select"
                    />
                </ModalBody>
                <ModalFooter className="border-top-0">
                    <Button disabled={addId.length < 1 ||submitting} color="netis-primary" style={{ borderRadius: '6px' }} className="px-3" onClick={submitAdmin}>
                        {submitting ? <Spinner size="sm" color="light" className="mx-auto" /> : 'Submit'}
                    </Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={deleting ? true : false} toggle={() => setDeleting(null)} style={{marginTop: '40vh'}}>
                <ModalBody className="text-center pt-5">
                    Apakah anda yakin untuk menghapus akun {deleting?.detail?.fullName} dari daftar Admin ?
                </ModalBody>
                <ModalFooter className="border-top-0">
                <Button disabled={submitting} color="outline-netis-primary" style={{ borderRadius: '6px' }} className="px-3 mr-2" onClick={() => setDeleting(null)}>Batal</Button>
                    <Button disabled={submitting} color="danger" style={{ borderRadius: '6px' }} className="px-3 ml-2" onClick={deleteAdmin}>
                        {submitting ? <Spinner size="sm" color="light" className="mx-auto" /> : 'Hapus'}
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default Admin