import React, { useState } from 'react'
import DataNotFound from '../../../components/DataNotFound';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile'
import ProfilePhotoNotFound from '../../../assets/img/no-photo.png'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Spinner, Table } from 'reactstrap';
import request from '../../../utils/request';
import { toast } from 'react-toastify';

const statDesc = {
    basic: 'menonaktifkan',
    blocked: 'mengaktifkan'
}
const actionButton = {
    basic: 'Nonaktifkan',
    blocked: 'Aktifkan'
}

function UserList({ data, getData, stat}){
    const [changeData, setChangeData] = useState(null)
    const [verify, setVerify] = useState(null)
    const [loading, setLoading] = useState(false)

    const cancelChangeStatus = () => {
        setChangeData(null)
        setVerify(null)
    }

    const changeStatus = () => {
        setLoading(true)
        request.put(`v1/users/${changeData?.id}/verify-status`, {status: verify})
        .then(() => {
            toast.success(`Berhasil ${statDesc[stat]} User`)
            setVerify(null)
            setChangeData(null)
            getData()
        })
        .catch(() => {
            toast.error(`Gagal ${statDesc[stat]} User`)
            return;
        })
        .finally(() => setLoading(false))
    }

    const onAvatarError = (e) => {
        const img = e.target;
        img.onerror = null;
        img.src = ProfilePhotoNotFound;
        img.style.border = null;
    }

    if (data.length < 1) {
        return <DataNotFound />
    }

    return(
        <>
            <Table hover responsive>
                <thead>
                    <tr className="bg-white border-bottom-1">
                        <th style={{ width: '40px' }}></th>
                        <th className="text-left">Nama</th>
                        <th className="text-left">No Telepon</th>
                        <th className="text-left">Email</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {data?.map((item, idx) => (
                        <tr key={idx}>
                            <td style={{ width: '40px', paddingLeft: '3px', paddingRight: '3px' }} className="text-center">
                                {item?.detail?.photo ?
                                    <img src={item.detail.photo} alt="profile" width={30} height={30} style={{ objectFit: 'cover' }} onError={(e) => onAvatarError(e)} className="rounded-circle border" />
                                    :
                                    <DefaultProfile init={item.detail.fullName} size="30px" />
                                }
                            </td>
                            <td>
                                <strong>{item?.detail?.fullName}</strong>
                            </td>
                            <td className="text-left">{item?.detail?.phoneNumber}</td>
                            <td><i>{item?.email}</i></td>
                            <td className="text-nowrap">
                                <Button
                                    onClick={() => {
                                        setVerify(stat === 'basic' ? 'blocked' : 'basic')
                                        setChangeData(item)
                                    }} 
                                    color="netis-primary"
                                    className="px-3"
                                    style={{ borderRadius: '8px' }}
                                >
                                    {actionButton[stat]}
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Modal isOpen={changeData ? true : false} style={{ marginTop: "40vh" }} toggle={cancelChangeStatus}>
                <ModalHeader className="border-bottom-0">
                    Perubahan Status User
                </ModalHeader>
                <ModalBody className="text-center py-2">
                    Apakah anda ingin {statDesc[stat]} User <b>"{changeData?.detail?.fullName}"</b> ?
                </ModalBody>
                <ModalFooter className="border-top-0 text-center py-3">
                    <div className="text-center d-flex justify-content-between mx-auto mt-2" style={{ width: "50%" }}>
                        <Button onClick={changeStatus} disabled={loading} color="netis-success" className="mr-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            {loading ? <Spinner size="sm" className="mx-auto" /> : <><i className="fa fa-check mr-1" />Ya</>}
                        </Button>
                        <Button onClick={cancelChangeStatus} disabled={loading} color="netis-danger" className="ml-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            <i className="fa fa-times mr-1" />Batal
                        </Button>
                    </div>
                </ModalFooter>
            </Modal>
        </>
    )
}

export default UserList