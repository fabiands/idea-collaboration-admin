export const notif = [
    {
        id:1,
        causer: "Alfonso Fred",
        request: {
            type: 'team',
            title: 'Mulya Sejahtera'
        },
        createdAt: '2021-05-17 03:40:00'
    },
    {
        id:2,
        causer: "Aldi Wahyu",
        request: {
            type: 'project',
            title: 'Perbaikan Jalan Sudirman'
        },
        createdAt: '2021-05-15 21:00:00'
    },
    {
        id:3,
        causer: "John Kohl",
        request: {
            type: 'team',
            title: 'Suka Kuli'
        },
        createdAt: '2021-05-17 12:30:00'
    }
]