import React, { useState } from 'react'
import { Badge, Button, Col, Form, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row, Spinner, Table } from 'reactstrap'
import DataNotFound from "../../../components/DataNotFound";
import request from '../../../utils/request';
import * as moment from 'moment'
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import noImageFound from '../../../assets/img/no-image-preview.png';

const badge = {
    registration: {
        color: 'warning', label: 'Level 2'
    },
    ideation: {
        color: 'success', label: 'Level 3'
    },
    finish: {
        color: 'info', label: 'Level 4'
    }
}

function ProjectList({ data, stat, getData }) {
    const [loading, setLoading] = useState(false)
    const [status, setStatus] = useState(null)
    const [changeValue, setChangeValue] = useState(null)
    const [changeCode, setChangeCode] = useState(null)
    const [list, setList] = useState(data)

    const toggleCancel = () => {
        setChangeValue(null)
        setStatus(null)
        setChangeCode(null)
    }
    const filterStatus = (e) => {
        if (!e) {
            setList(data)
        }
        else if (e) {
            const { value } = e;
            let filtered = data.filter(item => item.status === value)
            setList(filtered)
        }
    }
    const submitStatus = () => {
        setLoading(true)
        request.put(`v1/projects/${changeCode}/status`, { status: changeValue })
            .then(() => {
                toast.success('Berhasil Mengubah Status Project')
                getData()
            })
            .catch(() => {
                toast.error('Gagal mengubah Status Project')
                return;
            })
            .finally(() => setLoading(false))
    }
    const onErrorImage = (e) => {
        e.target.src = noImageFound;
        e.target.onerror = null;
    }
    const filterOption = [
        { value: 'registration', label: 'registration' },
        { value: 'ideation', label: 'ideation' },
        { value: 'finish', label: 'finish' }
    ]

    if (data?.length < 1) {
        return <DataNotFound />
    }
    return (
        <>
            {stat === 'accept' &&
                <Row className="mb-3">
                    <Col md="6">
                        <Select
                            isClearable={true}
                            options={filterOption}
                            onChange={filterStatus}
                        />
                    </Col>
                </Row>
            }
            <Table hover responsive>
                <thead>
                    <tr className="bg-white">
                        <th></th>
                        <th className="td-title">Nama Proyek</th>
                        <th>Pelapor</th>
                        {stat !== 'verif' && <th className={stat === 'verif' ? 'text-center' : ''}>Tanggal Diverifikasi</th>}
                        {stat !== 'reject' &&
                            <th className="text-center">{stat === 'accept' && 'Status'}</th>
                        }
                    </tr>
                </thead>
                <tbody>
                    {list?.map((item, idx) => (
                        <tr key={idx}>
                            <td className="text-center py-auto">
                                {item?.media[0]?.storage ?
                                    <img src={item?.media[0]?.storage} alt="project" style={{ borderRadius: '4px' }} width={35} height={35} onError={(e) => onErrorImage(e)} />
                                    :
                                    <img src={require('../../../assets/img/no-image-preview.png')} style={{ borderRadius: '4px' }} width={35} height={35} alt="project" />
                                }
                            </td>
                            <td className="td-title">
                                <Link to={`/project/detail/${item.code}`} className="text-dark">
                                    <b>{item.title}</b>
                                </Link>
                            </td>
                            <td className={!item.user.name ? 'text-center' : ''}>
                                <span className="text-capitalize">
                                    {item.user.name ? <><i className="fa fa-user-o mr-2" />{item.user.name}</> : "-"}
                                </span>
                            </td>
                            {stat !== 'verif' && <td className={!item.verifiedAt ? 'text-center' : ''}>
                                {item.verifiedAt ? <><i className="fa fa-clock-o mr-1" />{moment(item.verifiedAt).format('h:mm, Do MMMM YYYY')}</> : "-"}
                            </td>}
                            {stat !== 'reject' &&
                                <td className="text-center">
                                    {stat === 'verif' ?
                                        <Link to={`/project/detail/${item.code}`} className="btn btn-sm btn-netis-primary px-3 py-2 mr-1 text-nowrap" style={{ borderRadius: '8px' }}>
                                            Lihat Detail
                                        </Link>
                                        :
                                        <Badge color={badge[item.status]?.color} className="hover"
                                            onClick={() => {
                                                setChangeValue(item.status)
                                                setChangeCode(item.code)
                                                setStatus(item.status)
                                            }}
                                        >
                                            {badge[item.status]?.label}
                                        </Badge>
                                    }
                                </td>}
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Modal isOpen={changeCode ? true : false} style={{ marginTop: "40vh" }} toggle={toggleCancel}>
                <Form>
                    <ModalHeader className="border-bottom-0" toggle={toggleCancel}>
                        Perubahan Status Proyek
                    </ModalHeader>
                    <ModalBody className="py-3">
                        <div className="d-flex justify-content-between">
                            <Label className="input-label" htmlFor="stat">Pilih Status Project</Label>
                            <Badge color={badge[status]?.color} className="text-capitalize py-auto px-1" style={{ height: '18px' }}>
                                {badge[status]?.label}
                            </Badge>
                        </div>
                        <Select
                            name="stat"
                            id="stat"
                            value={filterOption.find(item => item.value === changeValue)}
                            options={filterOption}
                            isClearable={false}
                            isSearchable={false}
                            onChange={(e) => setChangeValue(e.value)}
                        />
                    </ModalBody>
                    <ModalFooter className="border-top-0 text-center py-3">
                        <Button onClick={submitStatus} disabled={loading || (changeValue === status)} className="btn btn-netis-primary" style={{ width: "100px", borderRadius: "10px" }} >
                            {loading ? <Spinner size="sm" className="mx-auto" /> : "Simpan"}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        </>
    )
}

export default ProjectList