import React, { useCallback, useEffect, useState } from 'react'
import { Link, useLocation } from "react-router-dom";
import { Col, Input, Nav, NavItem, NavLink, Row, Spinner, TabContent, TabPane } from 'reactstrap';
import request from '../../../utils/request';
import ProjectList from './ProjectList';

const tabs = {
    'unverified' : 'Belum diverifikasi',
    'accepted' : 'Disetujui',
    'rejected' : 'Ditolak'
}
const tabsArray = Object.keys(tabs);

function Project(){
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(null)
    const [verified, setVerified] = useState(data?.filter(item => item.verified === 'verified') ?? null)
    const [unverified, setUnverified] = useState(data?.filter(item => item.verified === 'pending') ?? null)
    const [rejected, setRejected] = useState(data?.filter(item => item.verified === 'rejected') ?? null)
    const [filter, setFilter] = useState(null)
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    const getData = useCallback(() => {
        setLoading(true);
        return request.get('v1/projects')
            .then((res) => {
                setData(res.data.data)
                setFilter(res.data.data)
            })
            .catch((err) => console.log(err))
            .finally(() => setLoading(false))
    },[])

    useEffect(() => {
        setVerified(filter?.filter(item => item.verified === 'verified') ?? null)
        setUnverified(filter?.filter(item => item.verified === 'pending') ?? null)
        setRejected(filter?.filter(item => item.verified === 'rejected') ?? null)
    }, [filter])

    const searchData = (e) => {
        const {value} = e.target;
        if(value){
            let filtered = data.filter(item => item.title.toLowerCase().includes(value.toLowerCase()))
            setFilter(filtered)
        }
        else {
            setFilter(data)
        }
    }

    useEffect(() => {
        getData()
    }, [getData])

    return(
        <div className="p-2 px-lg-4 project-tab">
            <h4>Proyek</h4>
            <Row className="mb-3 mb-md-0 d-flex flex-row-reverse">
                <Col md="4" lg="3" className="input-tab">
                    <div className="relative-input search-addon">
                        <Input id="search" name="search" type="input" onChange={searchData} placeholder="Search..." className="form-control" />
                        <i className="fa fa-search icon-inside-left text-netis-primary" />
                    </div>
                </Col>
                <Col md="8" lg="9">
                    <Nav tabs>
                        {tabsArray.map(tab => (
                            <NavItem key={tab}>
                                <NavLink tag={Link} className="pt-2/5" active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                                    {tabs[tab]}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
            </Row>
            <TabContent activeTab={selectedTab}>
                <TabPane tabId="unverified">
                    {(!unverified || loading) ? 
                        <div className="text-center my-3">
                            <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
                        </div>
                        :
                        <ProjectList key={unverified} data={unverified} getData={getData} stat="verif" />
                    }
                </TabPane>
                <TabPane tabId="accepted">
                    {(!verified || loading) ? 
                        <div className="text-center my-3">
                            <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
                        </div>
                        :
                        <ProjectList key={verified} data={verified} getData={getData} stat="accept" />
                    }
                </TabPane>
                <TabPane tabId="rejected">
                    {(!rejected || loading) ? 
                        <div className="text-center my-3">
                            <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
                        </div>
                        :
                        <ProjectList key={rejected} data={rejected} getData={getData} stat="reject" />
                    }
                </TabPane>
            </TabContent>

        </div>
    )
}

export default Project