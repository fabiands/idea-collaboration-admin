import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody, CardFooter, CardHeader, Row, Col, Button, Collapse } from 'reactstrap'
import { DefaultProfile } from '../../../../components/Initial/DefaultProfile'
import StarRatingComponent from "react-star-rating-component";
// import ReactStars from "react-rating-stars-component";

function TeamProject({ data, onClick, stat }) {
    const [info, setInfo] = useState(false)

    return (
        <Card key={data} style={{ borderRadius: '5px' }}>
            <CardHeader className="bg-white border-bottom-0">
                <h5 className="mb-2 font-weight-bolder">
                    Tim {stat === 'finish' && <i className="fa fa-info-circle text-info mt-2 ml-2 hover" onClick={() => setInfo(!info)} />}
                </h5>
                <Collapse isOpen={info}>
                    Proyek sudah memasuki tahap penilaian akhir, silahkan memberikan penilaian Design Sprint 
                    milik masing-masing Tim dengan skala 1 sampai 5. Perlu diperhatikan bahwa nilai yang sudah diberikan 
                    tidak dapat diganti, dan pastikan hanya ada 1 Tim yang memiliki nilai tertinggi. <br />
                    <Button className="btn-sm mt-2 py-0 px-2 text-white" color="info" style={{borderRadius:'8px'}} onClick={() => setInfo(false)}>Mengerti</Button>
                </Collapse>
            </CardHeader>
            {data?.teams?.find(item => item.status === 'approved') ?
                <CardBody style={{ borderTop: '1px solid #c8ced3', maxHeight: '45vh', overflowY: 'scroll' }} className="text-left border-top-0 py-1">
                    {data?.teams?.filter(item => item.status === 'approved').map((item, idx) => (
                        <Card className="card-team border-0" style={{ borderRadius: '5px' }} key={idx}>
                            <CardBody className="px-3 py-4">
                                <Row>
                                    <Col xs="2" className="d-flex justify-content-center align-items-center">
                                        <DefaultProfile init={item.leadName} size="30px" />
                                        {/* <img src={require('../../../../assets/img/no-photo.png')} alt="Pic" width={30} height={30} /> */}
                                    </Col>
                                    <Col xs="6" className="d-flex align-items-center">
                                        <div>
                                            {data?.status === 'registration' ? 
                                                <span className="text-dark"><b>Tim {item.leadName}</b></span>
                                            :
                                                <Link to={{
                                                    pathname: `/team/${data.code}/sprint/${item?.id}`,
                                                    state: {
                                                        project: data?.title,
                                                        team: item?.leadName,
                                                        stat: data?.status
                                                    }
                                                }}
                                                    className="text-dark">
                                                    <b>Tim {item.leadName}</b>
                                                </Link>
                                            }

                                            {data.status === 'finish' && 
                                                <>
                                                    <br />
                                                    {item.score ?
                                                        <StarRatingComponent
                                                            name="score"
                                                            editing={false}
                                                            starCount={5}
                                                            value={item.score ?? 0}
                                                        />
                                                    :
                                                        <small className="text-secondary">
                                                            <i>Belum ada penilaian</i>
                                                        </small>
                                                    }
                                                </>
                                            }
                                        </div>
                                    </Col>
                                    {data.status === 'finish' && !item.score && 
                                        <Col xs="4" className="d-flex align-items-center justify-content-end">
                                            <Button color="primary" onClick={() => onClick(item)}>
                                                Beri nilai
                                            </Button>
                                        </Col>
                                    }
                                </Row>
                            </CardBody>
                        </Card>
                    ))}
                </CardBody>
                : <CardBody className="text-center py-3">
                    Belum ada Tim yang disetujui untuk Proyek ini
                </CardBody>
            }
            <CardFooter className="border-top-0 bg-white"></CardFooter>
        </Card >
    )
}

export default TeamProject