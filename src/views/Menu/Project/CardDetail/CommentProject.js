import React, { useState } from 'react'
import { Card, CardHeader, CardFooter, CardBody, Row, Col, Button, Modal, ModalBody, ModalFooter, Spinner } from 'reactstrap'
import * as moment from 'moment'
import request from '../../../../utils/request'
import { toast } from 'react-toastify'
import { DefaultProfile } from '../../../../components/Initial/DefaultProfile'
import noProfile from '../../../../assets/img/no-photo.png'

function CommentProject({data, code}){
    const [comment, setComment] = useState(data)
    const [deletingId, setDeletingId] = useState(null)
    const [deleting, setDeleting] = useState(false)

    const deleteComment = () => {
        setDeleting(true)
        request.delete(`v1/projects/${code}/comment/${deletingId}`)
            .then(() => {
                setComment(comment.filter(item => item.id !== deletingId))
                setDeletingId(null)
            })
            .catch(() => toast.error('Gagal menghapus komentar'))
            .finally(() => setDeleting(false))
    }

    const onErrorImage = (e) => {
        e.target.src = noProfile;
        e.target.onerror = null;
    }

    return(
        <>
        <Card style={{ borderRadius: '5px' }}>
            <CardHeader className="bg-white border-bottom-0">
                <h5 className="mb-2 font-weight-bolder">Komentar</h5>
            </CardHeader>
            {data?.length < 1 ?
                <CardBody className="text-center py-3">
                    Belum ada Komentar untuk Proyek ini
                </CardBody>
            :
            <CardBody style={{ borderTop: '1px solid #c8ced3', maxHeight: '50vh', overflowY: 'scroll' }} className="text-left border-top-0 py-0">
                {comment?.map((item, idx) => (
                    <Row key={idx} className="pl-0">
                        <Col xs="2" className="d-flex justify-content-center align-items-center px-0">
                            {item.userPhoto ?
                                <img src={item.userPhoto} width={35} height={35} alt="profile" className="rounded-circle" onError={(e) => onErrorImage(e)} style={{objectFit:'cover'}} />
                                :
                                <DefaultProfile init={item.userFullName} size="35px" />
                            }
                        </Col>
                        <Col xs="10" className="pl-0 m-auto">
                            <Card style={{ borderRadius: "10px" }} className="bg-light m-0 my-2">
                                <CardBody className="pt-2 pb-4">
                                    <Row style={{position: 'relative'}}>
                                        <Button onClick={() => setDeletingId(item.id)} className="bg-transparent text-secondary btn-sm border-0" style={{position:'absolute', top:0, right:0}}>
                                            <i className="fa fa-trash" />
                                        </Button>
                                        <Col xs="10">
                                            <strong>{item.userFullName}</strong><br />
                                            <span>{item.comment}</span><br />
                                        </Col>
                                        <Col xs="12" className="text-right">
                                            <small className="text-secondary">{moment(item.createdAt).fromNow()}</small>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                ))}
            </CardBody>
            }
            <CardFooter className="border-top-0 bg-white pt-1">
            </CardFooter>
        </Card>
        <Modal isOpen={deletingId ? true : false} toggle={() => setDeletingId(null)}>
            <ModalBody className="text-center align-items-center pb-0">
                    Apakah anda yakin ingin menghapus komentar tersebut ?<br />
            </ModalBody>
            <ModalFooter className="border-top-0 text-center pb-4 pt-2">
                <div className="text-center d-flex justify-content-between mx-auto mt-2" style={{ width: "50%" }}>
                    <Button onClick={deleteComment} disabled={deleting} color="netis-primary" className="mr-2 px-2 btn-sm" style={{ width: "100px", borderRadius:'10px'}}>
                        {deleting ? <Spinner size="sm" className="mx-auto" /> : "Hapus"}
                    </Button>
                    <Button onClick={() => setDeletingId(null)} disabled={deleting} className="ml-2 px-2 button-reset btn btn-sm btn-outline-netis-primary" style={{ width: "100px", borderRadius:'10px'}}>
                        Batal
                    </Button>
                </div>
            </ModalFooter>
        </Modal>
        </>
    )
}

export default CommentProject