import React from 'react'
import { Row, Col } from 'reactstrap'
import GoogleMapReact from 'google-map-react';
import * as moment from 'moment'
import MapMarker from '../../../../components/MapMarker'

function DescProject({data, loc}){

    return(
        <Row form>
            <Col md="10">
                <strong>Nama Pelapor</strong><br />
                <span>{data?.user?.name ?? "-"}</span>
                <hr />
            </Col>
            <Col md="10">
                <strong>Lokasi</strong><br />
                <span className={loc ? 'mb-2' : ''}>{data?.locationName ?? "-"}</span>
                {loc && 
                    <div className="border mt-2" style={{ height: '30vh', width: '100%' }}>
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                            center={loc}
                            defaultZoom={11}
                        >
                            <MapMarker
                                lat={loc.lat}
                                lng={loc.lng}
                                text={""}
                            />
                        </GoogleMapReact>
                    </div>
                }
                <hr />
            </Col>
            <Col md="10">
                <strong>Waktu</strong><br />
                <span>{data.verifiedAt ? moment(data.verifiedAt).format('h:mm, Do MMMM YYYY') : "-"}</span>
                <hr />
            </Col>
            <Col md="10">
                <strong>Deskripsi</strong><br />
                <span>{data?.description ?? "-"}</span>
            </Col>
        </Row>
    )
}

export default DescProject