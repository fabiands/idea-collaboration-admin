import React, { useCallback, useEffect, useState } from 'react'
import { Badge, Button, Card, CardBody, Col, Form, Input, Label, ModalBody, ModalFooter, ModalHeader, Row, Spinner } from 'reactstrap'
import ModalError from '../../../components/ModalError'
import request from '../../../utils/request'
import Modal from 'reactstrap/lib/Modal'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router'
import Select from 'react-select';
import CommentProject from './CardDetail/CommentProject'
import DescProject from './CardDetail/DescProject'
import TeamProject from './CardDetail/TeamProject'
import CarouselProject from './Carousel/CarouselProject'
import ReactStars from "react-rating-stars-component";
import CardHeader from 'reactstrap/lib/CardHeader'

const stat = {
    verified: 'memverifikasi',
    rejected: 'menolak'
}

const badge = {
    registration: 'warning',
    ideation: 'success',
    finish: 'info'
}

function ProjectDetail({ match }) {
    const history = useHistory();
    const [data, setData] = useState(null)
    const [verify, setVerify] = useState(null)
    const [status, setStatus] = useState(null)
    const [loading, setLoading] = useState(true)
    const [loadingStatus, setLoadingStatus] = useState(false)
    const [loadingVerify, setLoadingVerify] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [loc, setLoc] = useState(null)
    const [message, setMessage] = useState(null)
    const [changeValue, setChangeValue] = useState(null)
    const [changeCode, setChangeCode] = useState(null)
    const [rateTeam, setRateTeam] = useState({
        isOpen: false,
        data: null
    })

    const toggleCancel = () => {
        setChangeValue(null)
        setChangeCode(null)
    }

    const getData = useCallback(() => {
        return request.get(`v1/projects/${match.params.id}`)
        .then((res) => {
            let item = res.data.data
            setData(item)
            setStatus(item.status)
            if (item.locationLatitude && item.locationLongitude) {
                setLoc({
                    lat: parseFloat(item.locationLatitude),
                    lng: parseFloat(item.locationLongitude)
                })
            }
        })
        .catch(() => setNotFound(true))
        .finally(() => setLoading(false))
    }, [match])


    useEffect(() => {
        getData()
    }, [getData])

    const verifyCode = () => {
        setLoadingVerify(true)
        request.put(`v1/projects/${match.params.id}/verify`, {
            verify: verify,
            message: message
        })
            .then(() => {
                toast.success(`Berhasil ${stat[verify]} Proyek`)
                history.push('/project')
            })
            .catch(() => {
                toast.error('Gagal mengubah verifikasi data')
                return;
            })
            .finally(() => setLoadingVerify(false))
    }

    const submitStatus = () => {
        setLoadingStatus(true)
        request.put(`v1/projects/${changeCode}/status`, { status: changeValue })
            .then(() => {
                getData();
                toast.success('Berhasil Mengubah Status Project')
                setStatus(changeValue)
                setChangeCode(null)
            })
            .catch(() => {
                toast.error('Gagal mengubah Status Project')
                return;
            })
            .finally(() => setLoadingStatus(false))
    }

    const filterOption = [
        { value: 'registration', label: 'registration' },
        { value: 'ideation', label: 'ideation' },
        { value: 'finish', label: 'finish' }
    ]

    const postRating = (score) => {
        request.post(`/v1/projects/${data.code}/team/${rateTeam.data.id}/rate`, { score })
            .then(() => {
                getData();
                toast.success('Berhasil memberi penilaian team finished');
                setRateTeam({ isOpen: !rateTeam.isOpen, data: null })
            })
            .catch((error) => {
                console.log(error)
                toast.errror('Gagal memberi penilaian team finished');
            })
    }

    if (loading) {
        return (
            <div className="text-center my-3">
                <Spinner color="dark" style={{ width: '3rem', height: '3rem' }} className="my-auto mx-auto" />
            </div>
        )
    }
    else if (notFound) {
        return <ModalError isOpen={true} />
    }

    return (
        <div className="px-md-3 project-detail">
            <h4 className="mb-3 text-capitalize">{data.title}</h4>
            <Row>
                <Col md={data?.verified === 'pending' ? '12' : '6'} className="px-0 px-md-3">
                    <Row>
                        <Col xs="12" className="px-0 px-md-3">
                            <Card className="bg-white p-2">
                                <CardBody>
                                    {data.verified === 'verified' &&
                                        <div className="text-right my-1">
                                            <Badge color={badge[status]} className="hover"
                                                onClick={() => {
                                                    setChangeValue(status)
                                                    setChangeCode(match.params.id)
                                                }}
                                            >
                                                {status}
                                            </Badge>
                                        </div>
                                    }
                                    <CarouselProject data={data?.media} height="40vh" type="picture-frame my-3" />
                                    <Row className="mt-3">
                                        <Col xs="5" className="pt-1 mt-2">
                                            {data.verified === 'verified' &&
                                                <>
                                                    <i className="fa fa-lg fa-arrow-up mr-2" />
                                                    <span className="mr-1">
                                                        {data.votes.filter(item => item.type === 'up').length}
                                                    </span>
                                                    <i className="fa fa-lg fa-arrow-down ml-3 mr-2" />
                                                    <span>
                                                        {data.votes.filter(item => item.type === 'down').length}
                                                    </span>
                                                </>
                                            }
                                        </Col>
                                        <Col xs="7" className="text-right mt-2">
                                            {data.verified === 'pending' &&
                                                <>
                                                    <Button onClick={() => setVerify('verified')} color="netis-success" className="mr-2 px-2" style={{ borderRadius: '10px' }}>
                                                        <i className="fa fa-check mr-1" />Terima
                                                    </Button>
                                                    <Button onClick={() => setVerify('rejected')} color="netis-danger" className="ml-2 px-2" style={{ borderRadius: '10px' }}>
                                                        <i className="fa fa-times mr-1" />Tolak
                                                    </Button>
                                                </>
                                            }
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="12" className="px-0 px-md-3">
                            <Card className="bg-white p-2">
                                <CardBody>
                                    <DescProject data={data} loc={loc} />
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Col>
                {data?.verified !== 'pending' && 
                    <Col md="6" className="px-0 px-md-3">
                        <Row>
                            {data?.verified !== 'rejected' ?
                                <>
                                    <Col xs="12" className="px-0 px-md-3">
                                        <TeamProject key={data} stat={status} data={data} onClick={(data) => setRateTeam({ isOpen: !rateTeam.isOpen, data })} />
                                    </Col>
                                    <Col xs="12" className="px-0 px-md-3">
                                        <CommentProject data={data.comments} code={match.params.id} />
                                    </Col>
                                </>
                            :
                            <Col xs="12">
                                <Card className="border-0 shadow-sm" style={{ borderRadius: '5px' }}>
                                    <CardHeader className="bg-white border-bottom-0">
                                        <h5 className="mb-1 font-weight-bolder">Alasan Penolakan</h5>
                                    </CardHeader>
                                    <CardBody style={{ borderTop: '1px solid #c8ced3', height: '20vh' }} className="text-left border-top-0 py-1">
                                        <h6>{data?.verifiedMessage ?? "-"}</h6>
                                    </CardBody>
                                </Card>
                            </Col>
                            }
                        </Row>
                    </Col>
                }
            </Row>
            <Modal isOpen={verify ? true : false} style={{ marginTop: "40vh" }} toggle={() => setVerify(null)}>
                <ModalHeader className="border-bottom-0">
                    Perubahan Status Verifikasi Proyek
                </ModalHeader>
                <ModalBody className="text-center py-2">
                    Apakah anda ingin {stat[verify]} project <b>"{data.title}"</b> ?
                    {verify === 'rejected' &&
                        <Form className="mt-4 text-left">
                            <Label htmlFor="message" className="input-label">Berikan alasan penolakan Project</Label>
                            <Input type="textarea" id="message" name="message" rows="3" onChange={(e) => setMessage(e.target.value)} />
                        </Form>
                    }
                </ModalBody>
                <ModalFooter className="border-top-0 text-center py-3">
                    <div className="text-center d-flex justify-content-between mx-auto mt-2" style={{ width: "50%" }}>
                        <Button onClick={verifyCode} disabled={loadingVerify || (verify === 'rejected' && !message)} color="netis-success" className="mr-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            {loadingVerify ? <Spinner size="sm" className="mx-auto" /> : <><i className="fa fa-check mr-1" />Ya</>}
                        </Button>
                        <Button onClick={() => setVerify(null)} disabled={loadingVerify} color="netis-danger" className="ml-2 px-2" style={{ width: "100px", borderRadius: '10px' }}>
                            <i className="fa fa-times mr-1" />Batal
                        </Button>
                    </div>
                </ModalFooter>
            </Modal>
            <Modal isOpen={changeCode ? true : false} style={{ marginTop: "40vh" }} toggle={toggleCancel}>
                <Form>
                    <ModalHeader className="border-bottom-0" toggle={toggleCancel}>
                        Perubahan Status Proyek
                    </ModalHeader>
                    <ModalBody className="py-3">
                        <div className="d-flex justify-content-between">
                            <Label className="input-label" htmlFor="stat">Pilih Status Project</Label>
                            <Badge color={badge[changeValue]} className="text-capitalize py-auto px-1" style={{ height: '18px' }}>
                                {changeValue}
                            </Badge>
                        </div>
                        <Select
                            name="stat"
                            id="stat"
                            value={filterOption.find(item => item.value === changeValue)}
                            options={filterOption}
                            isClearable={false}
                            isSearchable={false}
                            onChange={(e) => setChangeValue(e.value)}
                        />
                    </ModalBody>
                    <ModalFooter className="border-top-0 text-center py-3">
                        <Button onClick={submitStatus} disabled={loadingStatus || (changeValue === status)} className="btn btn-netis-primary" style={{ width: "100px", borderRadius: "10px" }} >
                            {loadingStatus ? <Spinner size="sm" className="mx-auto" /> : "Simpan"}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={rateTeam.isOpen ? true : false} style={{ marginTop: "40vh" }} toggle={() => setRateTeam({ isOpen: !rateTeam.isOpen, data: null })}>
                <ModalHeader>
                    <div>Beri nilai untuk team ini</div>
                </ModalHeader>
                <ModalBody className="py-5 d-flex justify-content-center">
                    <ReactStars
                        count={5}
                        onChange={(e) => postRating(e)}
                        size={40}
                        value={rateTeam?.data?.score ?? 0}
                        // isHalf={true}
                        emptyIcon={<i className="fa fa-star"></i>}
                        halfIcon={<i className="fa fa-star-half-alt"></i>}
                        fullIcon={<i className="fa fa-star"></i>}
                        activeColor="#ffd700"
                    />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default ProjectDetail