import React, { useCallback, useState } from 'react'
import { Carousel, CarouselControl, CarouselIndicators, CarouselItem } from 'reactstrap'
import noImageFound from '../../../../assets/img/no-image-preview.png';
import ReactPlayer from 'react-player/lazy'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function CarouselProject({data, height, type}){
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }
    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === data?.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }
    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? data?.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }
    const onErrorProject = (e) => {
        e.target.src = noImageFound;
        e.target.onerror = null;
    }
    const [videoPlayed, setVideoplayed] = useState({
        id: null
    })
    const handlePlayVideo = useCallback((id) => {
        setVideoplayed((state) => ({ ...state, id: id }))
    }, [])

    const handlePauseVideo = useCallback(() => {
        setVideoplayed((state) => ({ ...state, id: null }))
    }, [])


    return(
        <div className={`border mx-auto ${type}`} style={{height:height}}>
            <Carousel
                activeIndex={activeIndex}
                next={next}
                previous={previous}
                interval={false}
                className="carousel-post"
            >
                {data?.map((item, idx) => (
                    <CarouselItem
                        onExiting={() => setAnimating(true)}
                        onExited={() => setAnimating(false)}
                        key={idx}
                    >
                        {item.mimeType.includes('video') ?
                            <div className="position-relative w-100 h-100" onClick={() => videoPlayed.id !== item.id ? handlePlayVideo(item.id) : handlePauseVideo()} style={{ cursor: 'pointer' }}>
                                {videoPlayed.id !== item.id &&
                                    <FontAwesomeIcon className="position-absolute video-icon" icon="play" color="white" />
                                }
                                <ReactPlayer url={item.storage} playing={videoPlayed.id === item.id ? true : false} width="100%" height='100%' />
                            </div>
                            :
                            <img src={item.storage} alt={'media ' + (idx + 1)} width="100%" height="100%" style={{ objectFit: 'contain' }} onError={(e) => onErrorProject(e)} />
                        }
                    </CarouselItem>
                ))}
                {data?.length > 1 &&
                    <>
                        {activeIndex !== 0 && <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />}
                        {activeIndex !== data?.length - 1 && <CarouselControl direction="next" directionText="Next" onClickHandler={next} />}
                    </>
                }
            </Carousel>
            {data?.length > 1 &&
                <CarouselIndicators items={data} activeIndex={activeIndex} onClickHandler={goToIndex} />
            }
        </div>
    )
}

export default CarouselProject