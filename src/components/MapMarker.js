import React from 'react';

const MapMarker = ({onClick, text, data, active}) => {

    return (
        <div className="d-flex scale-map" style={style.wrapper} onClick={onClick}>
            {active ?
                <img src={require('../assets/assets_ari/pin-map-active.png')} className="hover" alt="" style={style.active} />
            :
                <img src={require('../assets/assets_ari/pin-map.png')} className="hover" alt="" style={style.icon} />
            }
            <h3 style={style.title}>{text}</h3>
        </div>
    );
}

const style = {
    wrapper: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: '15px',
        position: 'absolute',
        top: '-36px',
        left: '-11px',
    },
    active: {
        width: '24px',
        position: 'absolute',
        top: '-36px',
        left: '-11px',
    },
    title: {
        position: 'absolute',
        margin: '0',
        top: '-34px',
        left: '16px',
    }
};

export default MapMarker;
